let groupPerLen = 3;
let individualPerLen = 25;
let eleGap = 3;
let distance = n => (n - 1) >= 0 ? -Math.sqrt(n - 1) : Math.sqrt(1 - n);

var generateData = function(d, overall) {
    let _studentsRowData = d['studentsRowData'];

    let _studentLineData = [];
    let _gapQ = 25;
    let offsetX = 0;

    _studentsRowData.forEach(function(d, i) {
        if (i == 0) {
            if (d['numOfUnfold'] == 2) {
                offsetX = individualPerLen / 2;
            } else if (d['numOfUnfold'] == 1) {
                offsetX = individualPerLen / 2;
            } else {
                offsetX = groupPerLen / 2;
            }
        }
        var flag = 0;
        if (d['prev'].includes('PF')) flag += 1;
        if (d["last"].includes("PF")) flag += 1;
        var gap = distance(d.dis) * 10;

        let _dv = 0;

        if (d['numOfUnfold'] == 1) {
            _dv = (individualPerLen + groupPerLen) / 2
        } else if (d['numOfUnfold'] == 2) {
            _dv = individualPerLen
        } else {
            _dv = groupPerLen
        }
        if (d['segStatus'] == 'inter') {
            _dv += eleGap;
        }

        if (flag == 2) {
            _studentLineData.push({
                x: offsetX,
                y: 0
            });
            _studentLineData.push({
                x: offsetX,
                y: gap
            });
            offsetX += _dv;
            _studentLineData.push({
                x: offsetX,
                y: gap
            });
            _studentLineData.push({
                x: offsetX,
                y: 0
            });
        } else if (flag == 1) {
            let _dy = overall.indexOf(d["prev"]) < overall.indexOf(d[
                "last"]) ? 70 : 70
            _studentLineData.push({
                x: offsetX,
                y: 0
            });
            _studentLineData.push({
                x: offsetX,
                y: _dy
            });
            offsetX += _dv;
            _studentLineData.push({
                x: offsetX,
                y: _dy
            });
            _studentLineData.push({
                x: offsetX,
                y: 0
            });
        } else {

            _studentLineData.push({
                x: offsetX,
                y: 0
            });
            offsetX += _dv / 2;
            _studentLineData.push({
                x: offsetX,
                y: distance(d.dis) * 5
            });
            offsetX += _dv / 2;
            _studentLineData.push({
                x: offsetX,
                y: 0
            });
        }
    });
    return _studentLineData;
}

let drawIndividual = function(individualData, courseInfo) {

    d3.select("#individualTitle").style("display", "block");
    d3.select("#individualSvg svg").remove();
    d3.select("#individualSvg p").remove();
    d3.select("#individualControl svg").remove();

    var svgView = d3.select("#individualSvg").append("svg");

    var showPanel = true;
    var individualType;
    let simSign = false;
    var ageCondition = [],
        dateCondition = [],
        gradeCondition = [],
        lenCondition = [],
        simCondition = [];
    let similarityList = [];
    let simDistBars = [];
    let numOfSimRect = 20;

    let ctrlSize = 0;

    let idIndividualMap = {};
    individualData.forEach(function(d) {
        idIndividualMap[d['userId']] = d;
    });



    $("#individualFilter").click(function() {

        if (showPanel) {
            d3.select("#individualControl")
                .transition()
                .duration(600)
                .style("opacity", "1");
            showPanel = false;
        } else {
            d3.select("#individualControl")
                .transition()
                .duration(600)
                .style("opacity", "0");
            showPanel = true;
        }
    });


    $("#ballonView").click(function() {
        individualType = "ballon";
        drawIndividualLines(individualData, individualType);
        drawIndividualCtrl(courseInfo);
    });

    $("#chainView").click(function() {
        individualType = "chain";
        drawIndividualLines(individualData, individualType);
        drawIndividualCtrl(courseInfo);

        d3.select("#individualControl")
            .transition()
            .duration(600)
            .style("opacity", "1");
    });



    function drawIndividualLines(data, type) {
        d3.select("#individualSvg p").remove();
        d3.select("#individualLegend svg").remove();
        d3.select("#individualLegend text").remove();

        let courseName = dataService.courseName;
        var overall = CourseConfig[courseName]['overall'];
        var element_to_week = CourseConfig[courseName]['element_to_week'];
        var colorDomain = CourseConfig[courseName]['colorDomain'];
        var colorRange = CourseConfig[courseName]['colorRange'];

        var color_week = d3.scaleQuantize()
            .domain(colorDomain)
            .range(colorRange);
        //Add Legend in the LegendDiv
        var series = [];
        for (var i = 1; i <= 11; ++i)
            series.push(i + '');
        var svgLegend = d3.select("#individualLegend").append("svg")
            .attr("width", "1000px")
            .attr("height", "20px")
            .append("g");
        series.forEach(function(s, i) {
            svgLegend.append('text')
                .attr('x', i * 80 + 20)
                .attr('y', 10)
                .style("fill", "black")
                .attr("font-size", "10px")
                .text("W" + s);
            svgLegend.append('rect')
                .attr('fill', color_week(i))
                .attr('width', 40)
                .attr('height', 10)
                .attr('x', i * 80 + 50)
                .attr('y', 0);
        });


        //var subdata = data.filter(a => a.distance.length < 500);
        if (data.length > 50)
            var subdata = data.slice(0, 50)
        else {
            var subdata = data;
            if (subdata.length == 0) {
                d3.select("#individualSvg").append("p")
                    .style("text-align", "center")
                    .style("font-size", "30px")
                    .text("No Coresponding Data Selected");
                return;
            }
        }

        var maxLen = _.max(subdata, i => i["sequence"].length);
        var viewMargin = {
                top: 20,
                right: 10,
                bottom: 10,
                left: 0
            },
            viewlWidth = maxLen["sequence"].length * 50,
            viewHeight = subdata.length * 280 + viewMargin.bottom;

        svgView.attr("width", viewlWidth + viewMargin.left)
            .attr("height", viewHeight + viewMargin.top)
            .append("g")
            .attr("transform", "translate(" + viewMargin.left + "," + viewMargin.top + ")");

        if (type == "chain") {
            var student_layout_rows = svgView.selectAll('.student')
                .data(subdata, function(d) {
                    return d['userId'];
                })

            student_layout_rows.exit().remove();


            student_layout_rows
                .transition()
                .attr('transform', (d, p) => 'translate(0,' + (p * 150 + 80) + ')')
                .duration(1000)

            student_layout_rows = student_layout_rows
                .enter()
                .append('g')
                .attr('class', 'student')
                .merge(student_layout_rows);

            student_layout_rows
                .transition()
                .attr('transform', (d, p) => 'translate(0,' + (p * 150 + 80) + ')')
                .duration(1000)

            var line = d3.line()
                .x(function(d) {
                    return d['x'];
                })
                .y(function(d) {
                    return d['y'];
                });
            // Data segmentation
            subdata.forEach(function(segObj) {
                let sequence = segObj['sequence'];
                let _tempSeg = null;
                let segSequence = [];
                let currentType = null;
                for (let i = 0, ilen = sequence.length; i < ilen; i++) {
                    let ele = sequence[i];
                    let _currentType = element_to_week[overall.indexOf(ele)];
                    if (i == 0) {
                        _tempSeg = {
                            'segs': [],
                            'type': _currentType,
                            'status': 'fold'
                        }; //fold
                        _tempSeg.segs.push(ele);
                        currentType = _currentType
                    } else if (_currentType != currentType) {
                        segSequence.push(_tempSeg);
                        _tempSeg = {
                            'segs': [],
                            'type': _currentType,
                            'status': 'fold'
                        };
                        _tempSeg.segs.push(ele);
                        currentType = _currentType;
                    } else {
                        _tempSeg.segs.push(ele);
                    }

                    if (i == ilen - 1) {
                        segSequence.push(_tempSeg)
                    }

                }
                segObj['segSequence'] = segSequence;
            });



            subdata.forEach(function(rowObj) {
                updateRowPathData(rowObj);
            });


            let drawRowPath = function(e, d) {
                let _studetnLineData = generateData(d, overall);
                let _container = d3.select(e)

                _studetnLineData['userId'] = d['userId']

                var row_pannel = _container.selectAll('.polyline')
                    .data([_studetnLineData], function(d) {
                        return d['userId']
                    })

                row_pannel
                    .transition()
                    .attr('d', line)
                    .duration(1000)

                row_pannel = row_pannel.enter().append('path').attr('class', 'polyline').merge(
                    row_pannel)
                row_pannel
                    .style("fill", "none")
                    .transition()
                    .attr('d', line)
                    .duration(1000)
                    .attr('stroke-width', 1)
                    .attr('stroke', '#777')
                    .attr('opacity', 0.8)
            }
            let drawRow = function(e, d) {


                let _this = e;
                d3.select(e).selectAll('.sequenceElements').remove();
                let _rowContnainer = d3.select(e) //.append('g').attr('class', 'sequenceElements');

                //Generate pie data
                let sequence = d['sequence'];
                let typeMap = {};
                sequence.forEach(function(event) {
                    //color_week(element_to_week[overall.indexOf(event)] - 1)
                    let type = element_to_week[overall.indexOf(event)]
                    if (typeMap[type] == undefined) {
                        typeMap[type] = 0;
                    }
                    typeMap[type] += 1
                });
                let weekSeek = [];
                for (let attr in typeMap) {
                    weekSeek.push({
                        'type': attr,
                        'number': typeMap[attr]
                    })
                }
                var pie = d3.pie()
                    .sort(null)
                    .value(function(d) {
                        return d.number;
                    });


                var path = d3.arc()
                    .outerRadius(25 - 5)
                    .innerRadius(5);

                _rowContnainer.selectAll('.legendContainer').remove();
                let legendContainer = _rowContnainer.append('g').attr('class',
                    'legendContainer')

                var arc = legendContainer.selectAll(".arc")
                    .data(pie(weekSeek))
                    .enter().append("g")
                    .attr("class", "arc")
                    .attr('transform', 'translate(40, -40)')

                arc.append("path")
                    .attr("d", path)
                    .attr("fill", function(d) {
                        return color_week(parseInt(d['data']['type']) - 1);
                    });


                let circle = legendContainer.append('circle').attr('cx', 40).attr('cy', -40)
                    .attr('r', 5).attr('fill', '#777');
                legendContainer.append('text').attr('x', 70).attr('y', -20).text(function(d) {
                    return d.userId + ' / ' + d.grade
                })
                legendContainer.append('text').attr('x', 70).attr('y', -45).text(function(d) {
                    if (!d['individualDistance']) return ''
                    return "Similarity: " + d['individualDistance']
                })
                circle.on('click', function(d) {
                    dataService.getIndividualSimilarityFromBackend(d['userId'],
                        function(queryResult) {
                            simSign = true;
                            let newList = findList(queryResult, idIndividualMap);
                            similarityList = newList;
                            updateSimilarityBrushConainer(queryResult);
                            drawIndividualLines(newList, 'chain')
                        })
                })


                // let _sequenceContaienr = _rowContnainer.append('g')
                let _sequenceContaienr = _rowContnainer

                let offsetX = 0;
                // console.log('seg', d.segSequence)
                // _sequenceContaienr.selectAll('.segContainer').remove();


                let sequenceSegContainer = _sequenceContaienr.selectAll('.segContainer').data(
                    d.segSequence,
                    function(d, i) {
                        return i
                    })



                sequenceSegContainer = sequenceSegContainer
                    .enter()
                    .append("g").attr('class', 'segContainer').merge(sequenceSegContainer)


                sequenceSegContainer.transition().attr("transform", function(segsObj, p) {
                    let _seq = segsObj.segs;
                    if (segsObj.status == 'fold') {
                        let _offx = offsetX;
                        offsetX += (eleGap + _seq.length * groupPerLen)
                        return "translate(" + _offx + ",0)";
                    } else if (segsObj.status == 'unfold') {
                        let _offx = offsetX;
                        offsetX += (eleGap + _seq.length * individualPerLen)
                        return "translate(" + _offx + ",0)";
                    }

                }).duration(1000);



                sequenceSegContainer.each(function(segsObj) {
                    let className = 'seg-container-' + segsObj['status'];

                    let _container = d3.select(this) //.append('g').attr('class', className);

                    if (segsObj['status'] == 'fold') {
                        if (segsObj['currentStatus'] == segsObj['status']) return
                        _container.selectAll('.shape').transition().attr(
                            "transform",
                            function(d) {
                                return "translate(" + '0' + "0)";
                            }).duration(1000).remove()


                        segsObj['currentStatus'] = 'fold';
                        let rect = _container.append('rect').attr('width', segsObj.segs
                                .length * groupPerLen)
                            .attr('height', 6)
                            .attr('fill', function(segsObj) {
                                return color_week(segsObj['type'] - 1);
                            })
                            .attr('stroke', function(segsObj) {
                                return color_week(segsObj['type'] - 1);
                            })
                            .attr('opacity', 0)
                            .on('click', function() {
                                segsObj['status'] = 'unfold';
                                updateRowPathData(d);
                                drawRowPath(_this, d);
                                drawRow(_this, d);

                            })
                        rect
                            .transition()
                            .attr('opacity', '0.8')
                            .duration(1500)
                            .attr('y', -2.5)

                    } else {
                        if (segsObj['currentStatus'] == segsObj['status']) return
                        segsObj['currentStatus'] = 'unfold'
                        _container.selectAll('rect').remove();
                        let shapeContainers = _container.selectAll('.shape')
                            .data(segsObj.segs, function(d, i) {
                                return i;
                            })
                        shapeContainers.transition()
                            .attr('transform', function(d, i) {
                                let x = i * individualPerLen + 11;
                                return "translate(" + x + ',' + 0 + ')';
                            })
                            .duration(1000)

                        shapeContainers = shapeContainers
                            .enter()
                            .append('g')
                            .attr('class', 'shape').merge(shapeContainers)

                        shapeContainers.transition()
                            .attr('transform', function(d, i) {
                                let x = i * individualPerLen + 11;
                                return "translate(" + x + ',' + 0 + ')';
                            })
                            .duration(1000)


                        shapeContainers.append('path')
                            .attr('d', d3.symbol().type(function(d) {
                                if (d[0] == "V")
                                    return d3.symbolSquare;
                                else if (d[0] == "P")
                                    return d3.symbolCircle;
                                else if (d[0] == "D")
                                    return d3.symbolTriangle;
                            }).size(250))
                            .attr("fill", d => color_week(element_to_week[overall.indexOf(
                                d)] - 1))
                            .style("opacity", "0.7")
                            .style("stroke", function(d) {
                                if (d[0] == "P")
                                    return "black";
                            })
                            .style("stroke-width", function(d) {
                                if (d[0] == "P")
                                    return "2px";
                            })
                            .on('click', function() {
                                segsObj['status'] = 'fold'

                                updateRowPathData(d);
                                drawRowPath(_this, d);
                                drawRow(_this, d);
                            })
                        shapeContainers.append('text')
                            .attr('x', 0)
                            .text(d => d)
                            .attr('y', 2)
                            .style("font-size", "7px")
                            .style("font-weight", "bold")
                            .attr("fill", function(d) {
                                var bgColor = d3.color(color_week(
                                    element_to_week[overall.indexOf(d)] -
                                    1)).rgb();
                                var yiq = (bgColor.r * 299 + bgColor.g * 587 +
                                    bgColor.b * 114) / 1000;
                                return (yiq >= 128) ? 'black' : 'white';
                            })
                            .attr("text-anchor", "middle")
                            .style('point-events', 'none')
                            .style('cursor', 'none')
                    }
                });


            }

            student_layout_rows.each(function(d) {
                d['pathContainer'] = this;
                // d3.select(this).selectAll('.sequenceElements').remove();
                drawRow(this, d);
                drawRowPath(this, d);
            });


        } else if (type == "ballon") {

            student_layout_rows = svgView.selectAll('.student')
                .data(subdata)
                .enter()
                .append('g')
                .attr('transform', (d, p) => 'translate(0,' + (p * 100 + 50) + ')');

            var xBottom = [],
                xUp = [];
            student_layout_rows.selectAll('path')
                .data(function(d) {
                    var rtd = [];
                    for (var i = 0; i < d.distance.length; ++i) {
                        t = {};
                        t["prev"] = d.sequence[i];
                        t["dis"] = d.distance[i];
                        t["last"] = d.sequence[i + 1];
                        rtd.push(t)
                    }
                    var x = 60,
                        str, tBottom = [],
                        tUp = [];
                    for (var i = 0; i < rtd.length; ++i) {
                        var flag = 0;
                        if (rtd[i]["prev"].includes("PF"))
                            flag += 1;
                        if (rtd[i]["last"].includes("PF"))
                            flag += 1;

                        str = "M ";
                        if (rtd[i].dis == 1) {
                            tBottom.push(x);
                            str += x + ' 0 L ' + x + " -25";
                            tUp.push(x);
                            x += 20;
                        }
                        if (rtd[i].dis < 1) {
                            if (flag == 1) {
                                tBottom.push(x);
                                str += x + ' 0 L ' + (x + 40) + ' -25';
                                tUp.push(x + 40);
                                x += 40 + 20;
                            } else {
                                tBottom.push(x);
                                str += x + ' 0 L ' + (x + 2.5 * Math.round(Math.sqrt(Math.abs(
                                    rtd[i].dis)))) + ' -25';
                                tUp.push(x + 2.5 * Math.round(Math.sqrt(Math.abs(rtd[i].dis))));
                                x += 2.5 * Math.round(Math.sqrt(Math.abs(rtd[i].dis))) + 20;
                            }
                        }
                        if (rtd[i].dis > 1) {
                            if (flag == 1) {
                                tUp.push(x);
                                str += x + ' -25 L ' + (x + 40) + ' 0';
                                tBottom.push(x + 40);
                                x += 40 + 20;
                            } else {
                                tUp.push(x);
                                str += x + ' -25 L ' + (x + 2.5 * Math.round(Math.sqrt(rtd[
                                    i].dis))) + ' 0';
                                tBottom.push(x + 2.5 * Math.round(Math.sqrt(rtd[i].dis)));
                                x += 2.5 * Math.round(Math.sqrt(rtd[i].dis)) + 20;
                            }
                        }
                        rtd[i]["path"] = str;
                    }
                    xBottom.push(tBottom);
                    xUp.push(tUp);
                    return rtd;
                })
                .enter()
                .append('path')
                //.attr("class", "link")
                .style("stroke", "black")
                .style("fill", "none")
                .attr('d', function(d, p) {
                    return d["path"];
                })
                .attr("class", function(d) {
                    var flag = 0;
                    if (d["prev"].includes("PF"))
                        flag += 1;
                    if (d["last"].includes("PF"))
                        flag += 1;
                    if (flag == 1)
                        return 'finalJump';
                });
            d3.selectAll(".finalJump").style("stroke-dasharray", "5,5");

            student_layout_rows.selectAll(".shapes")
                .data(function(d, p) {
                    var rtd = [];
                    for (var i = 0; i < d.sequence.length - 1; ++i) {
                        t = {};
                        t["TYPE"] = d.sequence[i];
                        t["x"] = xUp[p][i];
                        rtd.push(t);
                    }
                    return rtd;
                })
                .enter().append("path")
                .attr('d', d3.symbol().type(function(d) {
                    if (d["TYPE"][0] == "V")
                        return d3.symbolSquare;
                    else if (d["TYPE"][0] == "P")
                        return d3.symbolCircle;
                    else if (d["TYPE"][0] == "D")
                        return d3.symbolTriangle;
                }).size(180))
                .attr("transform", function(d) {
                    return "translate(" + d["x"] + ",-25)";
                })
                .attr("fill", d => color_week(element_to_week[overall.indexOf(d["TYPE"])] - 1))
                .style("opacity", "0.7")
                .style("stroke", function(d) {
                    if (d["TYPE"][0] == "P")
                        return "black";
                })
                .style("stroke-width", function(d) {
                    if (d["TYPE"][0] == "P")
                        return "1px";
                });

            student_layout_rows.selectAll('text')
                .data(function(d, p) {
                    var rtd = [];
                    for (var i = 0; i < d.sequence.length - 1; ++i) {
                        t = {};
                        t["TYPE"] = d.sequence[i];
                        t["x"] = xUp[p][i];
                        rtd.push(t);
                    }
                    return rtd;
                }).enter()
                .append('text')
                .attr('x', d => d["x"])
                .text(function(d) {
                    if (d["TYPE"][0] == "V")
                        return d["TYPE"].slice(1);
                    if (d["TYPE"][0] == "P") {
                        s = d["TYPE"].slice(2);
                        if (s[0] == "0")
                            return s.slice(1);
                        else
                            return s;
                    } else
                        return d["TYPE"];
                })
                .attr('y', -23)
                .style("font-size", "7px")
                .style("font-weight", "bold")
                .style("fill", function(d) {
                    var bgColor = d3.color(color_week(element_to_week[overall.indexOf(d[
                            "TYPE"])] -
                        1)).rgb();
                    var yiq = (bgColor.r * 299 + bgColor.g * 587 + bgColor.b * 114) / 1000;
                    return (yiq >= 128) ? 'black' : 'white';
                })
                .attr("text-anchor", "middle");

            subdata.forEach(function(d, i) {
                svgView.append('text')
                    .attr('x', 0)
                    .attr('y', function() {
                        return i * 100;
                    })
                    .text(d.userId + " / " + d.grade)
                    .style("fill", "black")
                    .style("font-size", "15px");
            });

        }
    }

    function updateSimilarityBrushConainer(dataList) {
        ctrlSize = d3.select('#individual').node().getBoundingClientRect().width;
        simDistBars = [];
        var ctrlMargin = {
                top: 0,
                left: 20,
                bottom: 10,
                right: 10
            },
            ctrlWidth = ctrlSize - ctrlMargin.left - ctrlMargin.right,
            ctrlHeight = 160 - ctrlMargin.top - ctrlMargin.bottom;

        for (var i = 0; i < numOfSimRect; i++) {
            simDistBars[i] = 0
        }

        if (dataList != undefined) {
            dataList.forEach(function(d) {
                let val = d['val'];
                let index = parseInt(val * 100 / 100 * numOfSimRect);
                simDistBars[index] += 1
            })
        }

        let sum = 0;
        simDistBars.forEach(function(d) {
            sum += d;
        });

        var colorScale = d3.scaleLinear()
            .domain([1, d3.max(simDistBars, function(d) {
                return Math.log(d);
            })])
            .range(["#dddddd", "#333333"])
        rectContainer.selectAll('rect').remove()
        rectContainer.selectAll('rect')
            .data(simDistBars)
            .enter()
            .append('rect')
            .attr('class', 'similarityRect')
            .attr('y', -15)
            .attr('x', function(d, i) {
                return i * ctrlWidth / numOfSimRect;
            })
            .attr('width', ctrlWidth / numOfSimRect)
            .attr('height', 15)
            .attr('fill', function(d) {
                if (sum == 0)
                    return '#ddd'
                else
                    return colorScale(d);
            })
            .attr('stroke', 'white')
    }

    function drawIndividualCtrl(d) {
        ctrlSize = d3.select('#individual').node().getBoundingClientRect().width;
        d3.select("#individualControl svg").remove();
        d3.select("#individualControl").style("opacity", "0");

        var ctrlMargin = {
                top: 0,
                left: 20,
                bottom: 10,
                right: 10
            },
            ctrlWidth = ctrlSize - ctrlMargin.left - ctrlMargin.right,
            ctrlHeight = 200 - ctrlMargin.top - ctrlMargin.bottom;


        var svgControl = d3.select("#individualControl").append("svg")
            .attr("width", ctrlWidth + ctrlMargin.left + ctrlMargin.right)
            .attr("height", ctrlHeight + ctrlMargin.top + ctrlMargin.bottom)
            .attr("transform", "translate(0,0)")
            .append("g")
            .attr("transform", "translate(" + ctrlMargin.left + "," + ctrlMargin.top + ")");

        var xDate = d3.scaleTime()
            .domain([new Date(d.time.start * 1000), new Date(d.time.end * 1000)])
            .rangeRound([0, ctrlWidth]);
        var xLinear = d3.scaleLinear()
            .domain([0, 100])
            .range([0, ctrlWidth]);
        var domainLen = [];
        for (var i = 0; i <= 10; ++i)
            domainLen.push(i * 50 + '');
        domainLen.push("500+");
        var xLen = d3.scalePoint()
            .domain(domainLen)
            .range([0, ctrlWidth]);
        var lenLinear = d3.scaleLinear().domain([0, ctrlWidth]).range([0, 550]);


        let lengthBrushOffsetY = ctrlHeight - 120;
        let dateBrushOffsetY = ctrlHeight - 80;
        let gradeBrushOffsetY = ctrlHeight - 40;
        //let ageBrushOffsetY = ctrlHeight - 10;
        let simirityBrushOffsetY = ctrlHeight - 160;

        //similarity
        let similarityLinear = d3.scaleLinear()
            .domain([0, 1])
            .range([0, ctrlWidth]);

        let numOfSimRect = 20;

        // Global
        rectContainer = svgControl.append('g')
            .attr("class", "axis axis--grid")
            .attr("transform", "translate(0," + (simirityBrushOffsetY) + ")")
            //////////////////////////////////////////////////////////////////////////////////////////////////

        // svgControl.append("g")
        //     .attr("class", "axis axis--grid")
        //     .attr("transform", "translate(0," + (simirityBrushOffsetY) + ")")
        //     .call(d3.axisBottom(similarityLinear)
        //         .ticks(20)
        //         .tickSize(-15)
        //         .tickFormat(function() {
        //             return null;
        //         }));

        svgControl.append("g")
            .attr("transform", "translate(0," + (simirityBrushOffsetY) + ")")
            .call(d3.axisBottom(similarityLinear)
                .ticks(10)
                .tickPadding(0))
            .attr("text-anchor", "middle");

        svgControl.append("g")
            .attr("class", "brush")
            .attr("transform", "translate(0," + (simirityBrushOffsetY - 15) + ")")
            .call(d3.brushX()
                .extent([
                    [0, 0],
                    [ctrlWidth, 15]
                ])
                .on("end", similarityBrushend)
            );

        svgControl.append("text")
            .attr("transform", "translate(-10," + (simirityBrushOffsetY - 15) + ") rotate(90)")
            .attr("font-size", "10px")
            .text("Sim");
        updateSimilarityBrushConainer()

        function similarityBrushend() {
            if (!d3.event.sourceEvent) return;
            if (!d3.event.selection) return;
            var s = d3.event.selection;
            if (s == null) {
                return
            }
            var extent = s.map(similarityLinear.invert);
            simCondition = extent;

            drawIndividualLines(getFiltered(dateCondition, gradeCondition, lenCondition,
                    simCondition),
                individualType);

        } { // Length Brush
            svgControl.append("g")
                .attr("class", "axis axis--grid")
                .attr("transform", "translate(0," + (lengthBrushOffsetY) + ")")
                .call(d3.axisBottom(xLen)
                    .ticks(11)
                    .tickSize(-15)
                    .tickFormat(function() {
                        return null;
                    }));
            svgControl.append("g")
                .attr("transform", "translate(0," + (lengthBrushOffsetY) + ")")
                .call(d3.axisBottom(xLen)
                    .ticks(11)
                    .tickPadding(0))
                .attr("text-anchor", "middle");

            svgControl.append("g")
                .attr("class", "brush")
                .attr("transform", "translate(0," + (lengthBrushOffsetY - 15) + ")")
                .call(d3.brushX()
                    .extent([
                        [0, 0],
                        [ctrlWidth, 15]
                    ])
                    .on("end", lengthBrushend)
                );
            svgControl.append("text")
                .attr("transform", "translate(-10," + (lengthBrushOffsetY - 15) +
                    ") rotate(90)")
                .attr("font-size", "10px")
                .text("Len")

            // Date Brush
            svgControl.append("g")
                .attr("class", "axis axis--grid")
                .attr("transform", "translate(0," + (dateBrushOffsetY) + ")")
                .call(d3.axisBottom(xDate)
                    .ticks(d3.timeDay)
                    .tickSize(-15)
                    .tickFormat(function() {
                        return null;
                    }));

            svgControl.append("g")
                .attr("transform", "translate(0," + (dateBrushOffsetY) + ")")
                .call(d3.axisBottom(xDate)
                    .ticks(d3.timeWeek)
                    .tickPadding(0)
                    .tickFormat(d3.timeFormat("%b %d")))
                .attr("text-anchor", "middle");

            svgControl.append("g")
                .attr("class", "brush")
                .attr("transform", "translate(0," + (dateBrushOffsetY - 15) + ")")
                .call(d3.brushX()
                    .extent([
                        [0, 0],
                        [ctrlWidth, 15]
                    ])
                    .on("end", dateBrushend)
                );
            svgControl.append("text")
                .attr("transform", "translate(-10," + (dateBrushOffsetY - 15) + ") rotate(90)")
                .attr("font-size", "10px")
                .text("Date")

            // Grade Brush
            svgControl.append("g")
                .attr("class", "axis axis--grid")
                .attr("transform", "translate(0," + (gradeBrushOffsetY) + ")")
                .call(d3.axisBottom(xLinear)
                    .ticks(20)
                    .tickSize(-15)
                    .tickFormat(function() {
                        return null;
                    }));
            svgControl.append("g")
                .attr("transform", "translate(0," + (gradeBrushOffsetY) + ")")
                .call(d3.axisBottom(xLinear)
                    .ticks(10)
                    .tickPadding(0))
                .attr("text-anchor", "middle");
            svgControl.append("g")
                .attr("class", "brush")
                .attr("transform", "translate(0," + (gradeBrushOffsetY - 15) + ")")
                .call(d3.brushX()
                    .extent([
                        [0, 0],
                        [ctrlWidth, 15]
                    ])
                    .on("end", gradeBrushend)
                );
            svgControl.append("text")
                .attr("transform", "translate(-10," + (gradeBrushOffsetY - 15) + ") rotate(90)")
                .attr("font-size", "10px")
                .text("Grade")

            // Age Brush
            /*
             svgControl.append("g")
             .attr("class", "axis axis--grid")
             .attr("transform", "translate(0," + (ageBrushOffsetY) + ")")
             .call(d3.axisBottom(xLinear)
             .ticks(20)
             .tickSize(-15)
             .tickFormat(function() {
             return null;
             }));
             svgControl.append("g")
             .attr("transform", "translate(0," + (ageBrushOffsetY) + ")")
             .call(d3.axisBottom(xLinear)
             .ticks(10)
             .tickPadding(0))
             .attr("text-anchor", "middle");
             svgControl.append("g")
             .attr("class", "brush")
             .attr("transform", "translate(0," + (ageBrushOffsetY - 10) + ")")
             .call(d3.brushX()
             .extent([
             [0, 0],
             [ctrlWidth, 10]
             ])
             .on("end", ageBrushend)
             );
             svgControl.append("text")
             .attr("transform", "translate(-10," + (ageBrushOffsetY - 10) + ") rotate(90)")
             .attr("font-size", "10px")
             .text("Age");
             */

        }


        function lengthBrushend() {
            if (!d3.event.sourceEvent) return;
            if (!d3.event.selection) return;
            var d0 = [],
                d1 = [],
                moveto = [];
            d3.brushSelection(this).map(i => d0.push(Math.round(lenLinear(i))));
            d1.push(Math.floor(d0[0] / 50) * 50 + '');
            d1.push(Math.ceil(d0[1] / 50) * 50 + '');
            if (d1[1] == "550")
                d1[1] = "550+"
            d1.map(i => moveto.push(xLen(i)));
            if (moveto[1] == undefined)
                moveto[1] = ctrlWidth
            d3.select(this).transition().call(d3.event.target.move, moveto);
            lenCondition = d1;
            console.log("Individual Date selected");

            drawIndividualLines(getFiltered(dateCondition, gradeCondition, lenCondition,
                    simCondition),
                individualType);
            filteredInfo(dateCondition, gradeCondition, lenCondition);
        }
        /*
         function ageBrushend() {
         if (!d3.event.sourceEvent) return; // Only transition after input.
         if (!d3.event.selection) return; // Ignore empty selections.
         ageCondition = d3.brushSelection(this).map(xLinear.invert).map(i => Math.round(i));
         console.log("Individual Age Selected");

         drawIndividualLines(getFiltered(dateCondition, gradeCondition, ageCondition,
         lenCondition, simCondition),
         individualType);
         filteredInfo(dateCondition, gradeCondition, ageCondition, lenCondition);
         }
         */

        function gradeBrushend() {
            if (!d3.event.sourceEvent) return;
            if (!d3.event.selection) return;
            gradeCondition = d3.brushSelection(this).map(xLinear.invert).map(i => Math.round(i));
            console.log("Individual Grade Selected");
            console.log(gradeCondition);
            drawIndividualLines(getFiltered(dateCondition, gradeCondition, lenCondition,
                    simCondition),
                individualType);
            filteredInfo(dateCondition, gradeCondition, lenCondition);
        }

        function dateBrushend() {
            if (!d3.event.sourceEvent) return;
            if (!d3.event.selection) return;
            var d0 = d3.event.selection.map(xDate.invert),
                d1 = d0.map(d3.timeDay.round);
            // If empty when rounded, use floor & ceil instead.
            if (d1[0] >= d1[1]) {
                d1[0] = d3.timeDay.floor(d0[0]);
                d1[1] = d3.timeDay.offset(d1[0]);
            }
            d3.select(this).transition().call(d3.event.target.move, d1.map(xDate));
            dateCondition = d1.map(i => Date.parse(i) / 1000);
            console.log("Individual Date selected");

            drawIndividualLines(getFiltered(dateCondition, gradeCondition,
                    lenCondition, simCondition),
                individualType);
            filteredInfo(dateCondition, gradeCondition, lenCondition);
        }

        function getFiltered(date, grade, len, sim) {
            rtd = individualData;
            if (grade.length)
                rtd = _.filter(rtd, i => i.grade >= grade[0] && i.grade <= grade[1]);
            //if (age.length)
            //    rtd = _.filter(rtd, i => parseInt(i.age) >= age[0] && parseInt(i.age) <= age[1]);
            if (date.length) {
                temp = [];
                rtd.forEach(function(i) {
                    var timeRange = _.filter(i["time"], o => o >= date[0] && o <= date[
                        1]);
                    if (timeRange.length) {
                        var lower = i["time"].indexOf(timeRange[0]);
                        var upper = i["time"].indexOf(timeRange.slice(-1)[0]);
                        i["sequence"] = i["sequence"].slice(lower, upper + 1);
                        i["distance"] = i["distance"].slice(lower, upper);
                        i["distance_total"] = _.sum(i["distance"]);
                        i["time"] = timeRange;
                        temp.push(i)
                    }
                })
                rtd = temp;
            }
            if (len.length) {
                if (parseInt(len[1]) == 550)
                    rtd = _.filter(rtd, i => i["sequence"].length >= parseInt(len[0]));
                else
                    rtd = _.filter(rtd, i => i["sequence"].length >= parseInt(len[0]) && i[
                            "sequence"].length <=
                        parseInt(len[1]));
            }
            if (sim && sim.length) {

                rtd = _.filter(rtd, function(d) {
                    if (simSign == false) return true;
                    if (d['individualDistance'] && d['individualDistance'] > sim[0] &&
                        d['individualDistance'] < sim[1]) {
                        return true
                    }
                    return false
                })
            }
            rtd = _.filter(rtd, i => i["sequence"].length > 0);
            return rtd
        }

        function filteredInfo(date, grade, len) {
            rtd = "";
            if (date.length) {
                rtd += "Date: " + new Date(date[0] * 1000).toDateString().slice(4, 10) + " to " +
                    new Date(
                        date[
                            1] * 1000).toDateString().slice(4, 10) + " / ";
            }
            if (grade.length)
                rtd += "Grade: " + grade[0] + " to " + grade[1] + " / ";
            //if (age.length)
            //    rtd += "Age: " + age[0] + " to " + age[1] + " / ";
            if (len.length)
                rtd += "Len: " + len[0] + " to " + len[1] + " / ";
            if (rtd.length) {
                rtd = rtd.slice(0, -2);
                d3.select("#individualLegend")
                    .append("text")
                    .text(rtd)
            }
        }
    }

}


function findList(queryResult, idIndividualMap) {
    queryResult.sort(function(a, b) {
        return a['val'] - b['val'];
    });
    let tempArr = [];
    queryResult.forEach(function(result, i) {

        let id = result['userId'];
        let val = result['val'];
        if (idIndividualMap[id] != undefined) {

            idIndividualMap[id]['individualDistance'] = val;
            tempArr.push(idIndividualMap[id])
        }
    });
    return tempArr;
};

let updateRowPathData = function(rowObj) {
    let segObjSenquence = rowObj['segSequence']
    let _studentsRowData = [];
    let index = 0;

    for (var i = 0, ilen = segObjSenquence.length; i < ilen; i++) {
        let segs = segObjSenquence[i]['segs'];
        for (var j = 0, jlen = segs.length; j < jlen; j++) {
            let t = {
                'prev': segs[j],
                'dis': rowObj['distance'][index]
            };
            if (j != jlen - 1) {
                t['last'] = segs[j + 1];
                t['numOfUnfold'] = segObjSenquence[i]['status'] == 'unfold' ? 2 :
                    0;
                t['seqStatus'] = 'inner'
            } else {
                if (i + 1 == segObjSenquence.length) break
                t['last'] = segObjSenquence[i + 1]['segs'][0];
                t['segStatus'] = 'inter';
                if (segObjSenquence[i]['status'] == 'unfold' && segObjSenquence[i + 1]['status'] ==
                    'unfold') {
                    t['numOfUnfold'] = 2;
                } else if (segObjSenquence[i]['status'] == 'fold' && segObjSenquence[i + 1][
                        'status'
                    ] == 'fold') {
                    t['numOfUnfold'] = 0;
                } else {
                    t['numOfUnfold'] = 1;
                }
            }
            _studentsRowData.push(t);
            index += 1;
            if (index == rowObj.sequence.length) break
        }
    }
    rowObj['studentsRowData'] = _studentsRowData;
};
