/**
 * Created by qshen on 3/28/2017.
 */

var IndividualView = function(el){
    this.container = d3.select(el).append('g').attr('class', 'individual')
    this.signSim = false;
    this.ageCondition = [];
    this.dateCondition = [];
    this.gradeCondition = [];
    this.lenCondition = [];
    this.simCondition = [];
    this.individualType;
    this.showPanel = true;

    this.initContaienrs();
};

IndividualView.prototype.initData = function(individualData, courseInfo){
    let _this = this;
    this.individualData = individualData;
    this.courseInfo = courseInfo;
    this.idIndividualMap = {};
    this.individualData.forEach(function(d){
        _this.individualMap[d['userId']] = d;
    })

};
IndividualView.prototype.initContaienrs = function(){
    // Init for the rendering of sparklines
    this.groupPerLen = 3;
    this.individualPerLen = 25;
    this.eleGap = 3;


    d3.select("#individualTitle").style("display", "block");
    d3.select("#individualSvg svg").remove();
    // d3.select("#individualSvg p").remove();
    d3.select("#individualControl svg").remove();

    this.initButtons();
};

IndividualView.prototype.initButtons = function(){
    let _this = this;
    $("#individualFilter").click(function() {
        if (_this.showPanel) {
            d3.select("#individualControl")
                .transition()
                .duration(600)
                .style("opacity", "1");
            _this.showPanel = false;
        } else {
            d3.select("#individualControl")
                .transition()
                .duration(600)
                .style("opacity", "0");
            _this.showPanel = true;
        }
    });

    $("#ballonView").click(function() {
        _this.individualType = "ballon";
        _this.drawIndividualLines(_this.individualData, _this.individualType);
        _this.drawIndividualCtrl(_this.courseInfo);
    });

    $("#chainView").click(function() {
        _this.individualType = "chain";
        _this.drawIndividualLines(_this.individualData, _this.individualType);
        _this.drawIndividualCtrl(_this.courseInfo);
    });
};

IndividualView.prototype.findList = function(queryResult){
    let _this = this;
    queryResult.sort(function(a, b){
        return a['val'] - b['val'];
    });
    let tempArr = [];
    queryResult.forEach(function(result, i){

        let id = result['userId'];
        let val = result['val'];
        if(_this.idIndividualMap[id] != undefined){
            console.log('tell', id, val, result)
            _this.idIndividualMap[id]['individualDistance'] = val;
            tempArr.push(_this.idIndividualMap[id])
        }
    });
    return tempArr;
};

IndividualView.prototype.drawIndividualLines = function(data, type){
    let _this = this;
    d3.select("#individualSvg svg").remove();
    d3.select("#individualSvg p").remove();
    d3.select("#individualLegend svg").remove();
    d3.select("#individualLegend text").remove();

    if (dataService.courseName == "2014") {
        this.overall = ['V1.1', 'V1.2', 'V1.3', 'D1', 'V1.4', 'V1.5', 'V1.6', 'V1.7', 'V1.8', 'V1.9',
            'V1.10', 'V1.11', 'V1.12', 'PW01.1', 'PW01.2', 'V1.13', 'V1.14', 'PL01.1', 'V2.1', 'V2.2',
            'V2.3', 'PW02.1', 'V2.4', 'PW02.2', 'V2.5', 'V2.6', 'V2.7', 'PW02.3', 'V2.8', 'V2.9', 'V2.10',
            'PW02.4', 'D2', 'V2.11', 'V2.12', 'PL02.1', 'PL02.2', 'PL02.3', 'V3.1', 'V3.2', 'V3.3',
            'V3.4',
            'V3.5', 'V3.6', 'PW03.1', 'D3.1', 'V3.7', 'D3.2', 'V3.8', 'V3.9', 'V3.10', 'V3.11', 'V3.12',
            'PW03.2', 'V3.13', 'V3.14', 'V3.15', 'PW03.3', 'PW03.4', 'PL03.1', 'PL03.2', 'PL03.3',
            'PL03.4', 'V4.1', 'V4.2', 'V4.3', 'PW04.1', 'V4.4', 'V4.5', 'V4.6', 'V4.7', 'D4', 'V4.8',
            'PW04.2', 'V4.9', 'V4.10', 'V4.11', 'V4.12', 'PW04.3', 'PW04.4', 'V5.1', 'V5.2', 'V5.3',
            'PW05.1', 'V5.4', 'V5.5', 'V5.6', 'PW05.2', 'PW05.3', 'V5.7', 'PW05.4', 'PW05.5', 'V5.8',
            'D5',
            'V5.9', 'V5.10', 'V5.11', 'V5.12', 'V5.13', 'PL04.1', 'PL04.2', 'PL04.3', 'PL04.4', 'V6.1',
            'V6.2', 'PW06.1', 'V6.3', 'PW06.2', 'D6', 'V6.4', 'PW06.3', 'V6.5', 'V6.6', 'V6.7', 'PW06.4',
            'V6.8', 'PW06.5', 'V6.9', 'PW06.6', 'V6.10', 'V6.11', 'V7.1', 'V7.2', 'V7.3', 'PW07.1',
            'V7.4',
            'PW07.2', 'V7.5', 'PW07.3', 'V7.6', 'V7.7', 'V7.8', 'V7.9', 'PW07.4', 'V7.10', 'V7.11',
            'PL05.1', 'PL05.2', 'PL05.3', 'PL05.4', 'V8.1', 'V8.2', 'V8.3', 'PW08.1', 'V8.4', 'V8.5',
            'PW08.2', 'V8.6', 'V8.7', 'V8.8', 'V8.9', 'V8.10', 'V8.11', 'PW08.3', 'PL06.1', 'PL06.2',
            'PL06.3', 'PL06.4', 'PL06.5', 'V9.1', 'V9.2', 'V9.3', 'PW09.1', 'V9.4', 'PW09.2', 'V9.5',
            'PW09.3', 'D9', 'V9.6', 'V9.7', 'V9.8', 'V9.9', 'V9.10', 'V9.11', 'PW09.4', 'V10.1', 'V10.2',
            'V10.3', 'V10.4', 'PW10.1', 'V10.5', 'V10.6', 'V10.7', 'V10.8', 'V10.9', 'PW10.2', 'V10.10',
            'V10.11', 'V10.12', 'PF01', 'PF02', 'PF03', 'PF04', 'PF05', 'PF06', 'PF07', 'PF08', 'PF09',
            'PF10', 'PF11', 'PF12', 'PF13', 'PF14', 'PF15', 'PF16', 'PF17', 'PF18', 'PF19', 'PF20',
            'PF21',
            'PF22', 'PF23', 'PF24', 'PF25', 'PF26', 'PF27', 'PF28', 'PF29', 'PF30'
        ];
        var element_to_week = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2,
            2,
            2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
            3,
            3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5, 5, 5, 5,
            5,
            5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,
            6,
            7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
            8,
            8, 8, 8, 8, 8, 8, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 10, 10, 10, 10, 10, 10, 10,
            10, 10, 10, 10, 10, 10, 10, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11,
            11,
            11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11
        ];
        var colorDomain = [0, 11];
        var colorRange = ['#9e0142', '#d53e4f', '#f46d43', '#fdae61', '#fee08b', '#e6f598', '#abdda4',
            '#66c2a5',
            '#3288bd', '#5e4fa2', "#7D828A"
        ];
    } else if (dataService.courseName == "2015") {
        this.overall = ['V1.1', 'V1.2', 'V1.3', 'V1.4', 'V1.5', 'V1.6', 'V1.7', 'V1.8', 'V1.9', 'V1.10',
            'V1.11', 'PW01.1', 'PW01.2', 'V1.12', 'V1.13', 'PL01.1', 'V2.1', 'V2.2', 'V2.3', 'PW02.1',
            'V2.4', 'PW02.2', 'V2.5', 'V2.6', 'V2.7', 'PW02.3', 'V2.8', 'V2.9', 'V2.10', 'PW02.4', 'D2',
            'V2.11', 'V2.12', 'PL02.1', 'PL02.2', 'PL02.3', 'V3.1', 'V3.2', 'V3.3', 'V3.4', 'V3.5',
            'V3.6',
            'PW03.1', 'D3.1', 'V3.7', 'D3.2', 'V3.8', 'V3.9', 'V3.10', 'V3.11', 'V3.12', 'PW03.2',
            'V3.13',
            'V3.14', 'V3.15', 'PW03.3', 'PW03.4', 'PL03.1', 'PL03.2', 'PL03.3', 'PL03.4', 'V4.1', 'V4.2',
            'V4.3', 'PW04.1', 'V4.4', 'V4.5', 'V4.6', 'V4.7', 'D4', 'V4.8', 'V4.9', 'PW04.2', 'V4.10',
            'V4.11', 'V4.12', 'PW04.3', 'PW04.4', 'V4.13', 'PL04.1', 'PL04.2', 'PL04.3', 'V5.1', 'V5.2',
            'V5.3', 'PW05.1', 'V5.4', 'V5.5', 'V5.6', 'PW05.2', 'PW05.3', 'V5.7', 'PW05.4', 'PW05.5',
            'V5.8', 'D5.1', 'V5.8', 'V5.9', 'PW05.6', 'V5.10', 'PW05.7', 'D5.2', 'V5.11', 'PW05.8',
            'PL05.1', 'PL05.2', 'PF01', 'PF02', 'PF03', 'PF04', 'PF05', 'PF06', 'PF07', 'PF08', 'PF09',
            'PF10', 'PF11a', 'PF11b', 'PF11c', 'PF11d', 'PF11e', 'PF12A', 'PF12B', 'PF12C', 'PF12D',
            'PF12E', 'PF12F', 'PF12G', 'PF12H', 'PF12I', 'PF12J', 'PF13A', 'PF13B', 'PF13C', 'PF13D',
            'PF14', 'PF15A', 'PF15B'
        ];
        var element_to_week = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2,
            2,
            2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
            3,
            3, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5, 5,
            5,
            5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,
            6,
            6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6
        ];
        var colorDomain = [0, 6];
        var colorRange = ["#d7191c", "#fdae61", "#ffffbf", "#abdda4", "#2b83ba", "#7D828A"];
    }

    //var subdata = data.filter(a => a.distance.length < 500);
    if (data.length > 50){
        var subdata = data.slice(0, 50)
        _this.subdata = subdata;
    }
    else {
        var subdata = data;
        if (subdata.length == 0) {
            d3.select("#individualSvg").append("p")
                .style("text-align", "center")
                .style("font-size", "30px")
                .text("No Coresponding Data Selected");
            return;
        }
    }

    var maxLen = _.max(subdata, i => i["sequence"].length);
    var viewMargin = {
            top: 20,
            right: 10,
            bottom: 10,
            left: 0
        },
        viewlWidth = maxLen["sequence"].length * 50,
        viewHeight = subdata.length * 280 + viewMargin.bottom;
    var svgView = d3.select("#individualSvg").append("svg")
        .attr("width", viewlWidth + viewMargin.left)
        .attr("height", viewHeight + viewMargin.top)
        .append("g")
        .attr("transform", "translate(" + viewMargin.left + "," + viewMargin.top + ")");

    var color_week = d3.scaleQuantize()
        .domain(colorDomain)
        .range(colorRange);

    let maxLen = _.max(subdata, i => i['sequence'].length);
    var viewMargin = {
            top: 20,
            right: 10,
            bottom: 10,
            left: 0
        },
        viewlWidth = maxLen["sequence"].length * 50,
        viewHeight = subdata.length * 280 + viewMargin.bottom;

    var svgView = d3.select("#individualSvg").append("svg")
        .attr("width", viewlWidth + viewMargin.left)
        .attr("height", viewHeight + viewMargin.top)
        .append("g")
        .attr("transform", "translate(" + viewMargin.left + "," + viewMargin.top + ")");
    this.svgView = svgView;

    var color_week = d3.scaleQuantize()
        .domain(colorDomain)
        .range(colorRange);
    let color_week = color_week;

};

IndividualView.prototype.generatePathData = function(d){
    let _this = this;

    let groupPerLen = this.groupPerLen;
    let individualPerLen = this.individualPerLen;
    let eleGap = this.eleGap;

    let _studentsRowData = d['studentsRowData'];

    let _studentLineData = [];
    let _gapQ = 25;
    let offsetX = 0;

    _studentsRowData.forEach(function(d, i) {
        if (i == 0) {
            if (d['numOfUnfold'] == 2) {
                offsetX = individualPerLen / 2;
            } else if (d['numOfUnfold'] == 1) {
                offsetX = individualPerLen / 2;
            } else {
                offsetX = groupPerLen / 2;
            }
        }
        var flag = 0;
        if (d['prev'].includes('PF')) flag += 1;
        if (d["last"].includes("PF")) flag += 1;
        var gap = distance(d.dis) * 10;

        let _dv = 0;

        if (d['numOfUnfold'] == 1) {
            _dv = (individualPerLen + groupPerLen) / 2
        } else if (d['numOfUnfold'] == 2) {
            _dv = individualPerLen
        } else {
            _dv = groupPerLen
        }
        if (d['segStatus'] == 'inter') {
            _dv += eleGap;
        }

        if (flag == 2) {
            _studentLineData.push({
                x: offsetX,
                y: 0
            });
            _studentLineData.push({
                x: offsetX,
                y: gap
            });
            offsetX += _dv;
            _studentLineData.push({
                x: offsetX,
                y: gap
            });
            _studentLineData.push({
                x: offsetX,
                y: 0
            });
        } else if (flag == 1) {
            let _dy = _this.overall.indexOf(d["prev"]) < _this.overall.indexOf(d["last"]) ? 70 : 70
            _studentLineData.push({
                x: offsetX,
                y: 0
            });
            _studentLineData.push({
                x: offsetX,
                y: _dy
            });
            offsetX += _dv;
            _studentLineData.push({
                x: offsetX,
                y: _dy
            });
            _studentLineData.push({
                x: offsetX,
                y: 0
            });
        } else {

            _studentLineData.push({
                x: offsetX,
                y: 0
            });
            offsetX += _dv / 2;
            _studentLineData.push({
                x: offsetX,
                y: distance(d.dis) * 5
            });
            offsetX += _dv / 2;
            _studentLineData.push({
                x: offsetX,
                y: 0
            });
        }
    });
    return _studentLineData;


}


IndividualView.prototype.updateRowPathData = function(rowObj) {
    let segObjSenquence = rowObj['segSequence']
    let _studentsRowData = [];
    let index = 0;

    for (var i = 0, ilen = segObjSenquence.length; i < ilen; i++) {
        let segs = segObjSenquence[i]['segs'];
        for (var j = 0, jlen = segs.length; j < jlen; j++) {
            let t = {
                'prev': segs[j],
                'dis': rowObj['distance'][index]
            };

            if (j != jlen - 1) {
                t['last'] = segs[j + 1];
                t['numOfUnfold'] = segObjSenquence[i]['status'] == 'unfold' ? 2 : 0;
                t['seqStatus'] = 'inner'
            } else {
                if (i + 1 == segObjSenquence.length) break
                t['last'] = segObjSenquence[i + 1]['segs'][0];
                t['segStatus'] = 'inter';
                if (segObjSenquence[i]['status'] == 'unfold' && segObjSenquence[i + 1]['status'] ==
                    'unfold') {
                    t['numOfUnfold'] = 2;
                } else if (segObjSenquence[i]['status'] == 'fold' && segObjSenquence[i + 1]['status'] ==
                    'fold') {
                    t['numOfUnfold'] = 0;
                } else {
                    t['numOfUnfold'] = 1;
                }
            }
            _studentsRowData.push(t);
            index += 1;
            if (index == rowObj.sequence.length) break
        }
    }
    rowObj['studentsRowData'] = _studentsRowData;
};

IndividualView.prototype.drawRowPath = function(e, d) {
    let _this = this;
    let _studentLineData = _this.generatePathData(d);
    d3.select(e).selectAll('.lineContainer').remove();
    let _container = d3.select(e).append('g').attr('class', 'lineContainer')
    let row_pannel = _container.append('g').attr('transform','translate(50,0)');

    row_pannel
        .datum(_studentLineData)
        .append('path')
        .attr('class', (d, i) => 'path' + i)
        .style("fill", "none")
        .attr('d', line)
        .attr('stroke-width', 1)
        .attr('stroke', '#777')
        .attr('opacity', 0.8)
};

IndividualView.prototype.drawRow = function(e, d){

};

IndividualView.prototype.drawChain = function(){
    let _this = this;
    let groupPerLen = 3;
    let individualPerLen = 25;
    let eleGap = 3;

    this.student_layout_rows = this.svgView.selectAll('.student')
        .data(_this.subdata)
        .enter()
        .append('g')
        .attr('transform', (d, p) => 'translate(0,' + (p * 150 + 80) + ')');

    let distance = n => (n - 1) >= 0 ? -Math.sqrt(n - 1) : Math.sqrt(1 - n);
    var line = d3.line()
        .x(function(d) {
            return d['x'];
        })
        .y(function(d) {
            return d['y'];
        });
    subdata.forEach(function(segObj) {
        let sequence = segObj['sequence'];
        let _tempSeg = null;
        let segSequence = [];
        let currentType = null;
        for (let i = 0, ilen = sequence.length; i < ilen; i++) {
            let ele = sequence[i];
            let _currentType = element_to_week[overall.indexOf(ele)];
            if (i == 0) {
                _tempSeg = {
                    'segs': [],
                    'type': _currentType,
                    'status': 'fold'
                }; //fold
                _tempSeg.segs.push(ele);
                currentType = _currentType
            } else if (_currentType != currentType) {
                let _testType = null
                // if(i % 2 == 0) _testType = 'unfold';
                // else _testType = 'fold';
                segSequence.push(_tempSeg);
                _tempSeg = {
                    'segs': [],
                    'type': _currentType,
                    'status': 'fold'
                };
                _tempSeg.segs.push(ele);
                currentType = _currentType;
            } else {
                _tempSeg.segs.push(ele);
            }
        }
        segObj['segSequence'] = segSequence;
    });
    subdata.forEach(function(rowObj){
        _this.updateRowPathData(rowObj);
    })
};
