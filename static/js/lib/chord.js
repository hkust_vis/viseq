let drawChord = function(courseInfo, chordWeek, chordInWeek, type) {

    // THRESHOLD HANDLE
    var thresholdVal = 0;
    var switchToInput = function() {
        var $input = $("<input>", {
            val: $(this).text(),
            type: "text"
        });
        $input.addClass("loadNum");
        $(this).replaceWith($input);
        $input.on("blur", switchToSpan);
        $input.select();
    };
    var switchToSpan = function() {
        var $span = $("<span>", {
            text: thresholdVal = $(this).val()
        });
        $span.addClass("loadNum");
        $(this).replaceWith($span);
        $span.on("click", switchToInput);
        thresholdVal = +thresholdVal > 0 ? +thresholdVal : 0;
        console.log(thresholdVal)
        drawChordView(chordWeek, chordInWeek, thresholdVal);
    }
    $("#chord-threshold").on("click", switchToInput);

    // FILTER BUTTON
    var showPanel = true;
    if (type == "all") {
        $(".chordFilter").click(function() {
            console.log('Filter Chord Event')
            if (showPanel) {
                d3.select("#chordControlGrade")
                    .transition()
                    .duration(600)
                    .style("opacity", "0");
                d3.select("#chordControlPanel")
                    .transition()
                    .duration(600)
                    .style("opacity", "1");
                showPanel = false;
            } else {
                d3.select("#chordControlGrade")
                    .transition()
                    .duration(600)
                    .style("opacity", "1");
                d3.select("#chordControlPanel")
                    .transition()
                    .duration(600)
                    .style("opacity", "0");
                showPanel = true;
            }
        });
        drawChordView(chordWeek, chordInWeek, thresholdVal);
        drawChordCtrl(courseInfo);
    } else if (type == "filter") {
        drawChordView(chordWeek, chordInWeek, thresholdVal);
    }

    //REDRAW BUTTON
    $("#chord-redraw").on("click", function(d) {
        console.log('REDRAW');
        drawChordView(chordWeek, chordInWeek, threshold);
    });
    $("#chordDrawAll").on('click', function() {
        console.log('REDRAW ALL CHORD');
        drawChord(dataService.courseInfo, dataService.chordAllWeek, dataService.chordAllInWeek, "all");
    });


    function checkChordData(d) {
        t = [];
        Object.values(d).map(i => Object.values(i)).map(i => i.map(o => t.push(o)));
        return _.filter(t, i => i > 0);
    }

    function drawChordView(chordWeek, chordInWeek, threshold) {

        console.log(chordWeek);
        console.log(chordInWeek);

        d3.select("#chordSvg svg").remove();
        d3.select("#chordSvg p").remove();

        if (!checkChordData(chordWeek).length) {
            d3.select("#chordSvg").append("p")
                .style("text-align", "center")
                .style("font-size", "30px")
                .text("No Coresponding Data Selected");
            return;
        }

        var courseName = dataService.courseName;
        var overall = CourseConfig[courseName]['overall'];
        var element_to_week = CourseConfig[courseName]['element_to_week'];
        var colorDomain = CourseConfig[courseName]['colorDomain'];
        var colorRange = CourseConfig[courseName]['colorRange'];

        var element = document.getElementById("chord");
        var computedStyle = getComputedStyle(element);
        var height = element.clientHeight; // height with padding
        var width = element.clientWidth; // width with padding
        height -= parseFloat(computedStyle.paddingTop) + parseFloat(computedStyle.paddingBottom);
        width -= parseFloat(computedStyle.paddingLeft) + parseFloat(computedStyle.paddingRight);

        var viewWidth = width;
        var viewHeight = height - 120;
        console.log(width, height)
        var outerRadius = (d3.min([viewWidth, viewHeight]) - 50) / 2;
        var innerRadius = outerRadius - 20;
        var weekNums = Object.keys(chordWeek).length;

        var color = d3.scaleLinear()
            .domain(d3.range(weekNums).map(a => 2 * a))
            .range(colorRange)

        var chord = d3.chord()
            .padAngle(0)
            .sortSubgroups(d3.ascending)
            .sortChords(d3.descending);
        var arc_up = d3.arc()
            .innerRadius(innerRadius - 0.5)
            .outerRadius(outerRadius - 0.5);
        var arc_down = d3.arc()
            .innerRadius(innerRadius - 20)
            .outerRadius(outerRadius - 20);
        var ribbon = d3.ribbon()
            .radius(innerRadius);

        // ADD SVG ELEMENT
        var svgView = d3.select("#chordSvg").append("svg")
            .attr("width", viewWidth)
            .attr("height", viewHeight)
            .append("g")
            .attr("transform", "translate(" + viewWidth / 2 + "," + viewHeight / 2 + ")");

        // Clean the data, remove week1 to week1 values for example
        // Create an array data[][] where data[a][b] is the number of logs from week a to week b
        var raw = chordWeek;
        var data = Object.keys(raw).map(function(key, index) {
            return Object.keys(raw[key]).map((a, p) => (p === index) ? 0 : raw[key][a])
        });
        var n = d3.range(data.length * 2);
        var matrix_in_out = n.map(() => n.map(() => 0))
        data.map((d, p) => d.map((a, k) => {
            matrix_in_out[2 * p + 1][2 * k] = a
            matrix_in_out[2 * k][2 * p + 1] = a
        }));

        var matrix = chordInWeek;
        matrix = Object.keys(matrix).map(function(key, index) {
            return matrix[key]
        });

        get_group = n => Math.floor(n / 2) * 2;
        get_week = n => Math.floor(n / 2) + 1;

        var g = svgView.append("g")
            .datum(chord(matrix_in_out));

        // Inner ribbons
        var ribbons = g.append("g")
            .attr("class", "ribbons")
            .selectAll("path")
            .data(function(chords) {
                return chords;
            })
            .enter()
            .append("path")
            .attr("d", ribbon)
            .attr('class', 'ribbon-path')
            .style("fill", function(d) {
                return color(get_group(d.source.index % 2 != 0 ? d.source.index : d.target.index));
            })
            .attr("opacity", d => d.source.value < threshold ? 0 : d.source.value < 500 ? 0.2 : 0.5)
            .on("click", function(d) {
                var weekIdx = [];
                if (d.source.index % 2)
                    weekIdx.push(get_week(d.source.index), get_week(d.target.index));
                else
                    weekIdx.push(get_week(d.target.index), get_week(d.source.index));

                // TODO: Let the selected one opacity is 1. others remain same
                //d3.select(this).attr("opacity", "1")

                dataService.getIndividualFromBackend("chordFlow", weekIdx, function(grade) {
                    drawChordGrade(grade);
                });
            })
            .append("title")
            .text(function(d) {
                if (d.source.index % 2)
                    return "Week " + get_week(d.source.index) + " to Week " + get_week(d.target.index) +
                        ", Value : " + d
                        .source.value;
                else
                    return "Week " + get_week(d.target.index) + " to Week " + get_week(d.source.index) +
                        ", Value : " + d
                        .source.value;
            });

        var group = g.append("g")
            .attr("class", "groups")
            .selectAll("g")
            .data(function(chords) {
                return chords.groups;
            })
            .enter()
            .append("g");

        // Arcs Path
        group.append("path")
            .attr('class', 'outer')
            .style("fill", function(d) {
                return color(get_group(d.index));
            })
            .attr("d", d => d3.arc()
                .innerRadius(innerRadius - 15)
                .outerRadius(outerRadius - 15)(d))
            .on('click', expand)
            .on('mouseover', function() {
                d3.select(this).transition().duration(100).attr("d", d => d3.arc()
                    .innerRadius(innerRadius - 20)
                    .outerRadius(outerRadius)(d))
            })
            .on('mouseout', function() {
                d3.select(this).transition().duration(100).attr("d", d => d.index % 2 != 0 ?
                    arc_up(d) : arc_down(d))
            })
            .transition()
            .duration(800)
            .attr("d", d => d.index % 2 != 0 ? arc_up(d) : arc_down(d));

        // In & Out Text
        group.append("text")
            .each(function(d) {
                d.angle = (d.startAngle + d.endAngle) / 2;
            })
            .attr("dy", ".35em")
            .style("text-anchor", "middle")
            .style("fill", 'white')
            .text(function(d, p) {
                if (d.value)
                    return p % 2 == 0 ? ' In' : ' Out'
            })
            .attr("transform", function(d) {
                return "rotate(" + (d.angle * 180 / Math.PI - 90) + ")" + "translate(" + (
                    innerRadius - 5) + ")" + ((d.angle > Math.PI / 2 && d.angle < 3 *
                    Math.PI / 2) ? "rotate(-90)" : "rotate(90)");
            })
            .transition()
            .duration(800)
            .attr("transform", function(d) {
                return "rotate(" + (d.angle * 180 / Math.PI - 90) + ")" + "translate(" + (
                    innerRadius + (d.index % 2 != 0 ? 10 : -10)) + ")" + ((d.angle >
                        Math.PI / 2 && d.angle < 3 * Math.PI / 2) ? "rotate(-90)" :
                    "rotate(90)");
            });

        // Week Text
        group.append("text")
            .each(function(d) {
                d.angle = d.endAngle
            })
            .attr("dy", ".35em")
            .attr("transform", function(d) {
                return "rotate(" + (d.angle * 180 / Math.PI - 90) + ")" + "translate(" + (
                    innerRadius + 40) + ")" + ((d.angle > Math.PI / 2 && d.angle < 3 *
                    Math.PI / 2) ? "rotate(-90)" : "rotate(90)");
            })
            .style("text-anchor", "middle")
            .style('display', d => d.index % 2 != 0 ? 'none' : '')
            .text(function(d, p) {
                if (d.value)
                    return 'Week' + (Math.floor(p / 2) + 1);
            });



        function expand(d) {
            console.log(d)

            var stage = 0;
            var tau = 2 * Math.PI; // http://tauday.com/tau-manifesto
            var index_focuses = Math.floor(d.index / 2);

            var info_0 = chord(matrix_in_out).groups[d.index]
            console.log(info_0)

            var matrix_display = svgView.append('g')
                .attr('class', 'matrix_display')
                .on('click', function() {
                    // if (stage == 0) {
                    // 	show_rect(index_focuses)
                    // 	stage++
                    // } else {
                    // 	d3.selectAll('.matrix_display')
                    // 		.transition()
                    // 		.duration(800)
                    // 		.style('opacity', 0)
                    // 		.remove()
                    // }
                    d3.selectAll('.matrix_display')
                        .transition()
                        .duration(800)
                        .style('opacity', 0)
                        .remove()
                })
                //.style('cursor', 'pointer')

            // Outter Circle Path Arc
            matrix_display.selectAll('.transition')
                .data([info_0])
                .enter()
                .append("path")
                .attr('class', 'transition')
                .style("fill", "white")
                //.style("fill", color(get_group(info_0.index)))
                .attr("d", d3.arc())
            d3.select('.transition').transition().duration(600).attrTween("d", arcTween(tau))

            // Inner Circle Space
            matrix_display.append('rect')
                .attr('class', 'matrix_display_rect')
                .attr('width', 2 * (outerRadius + 30))
                .attr('height', 2 * (outerRadius + 30))
                .attr('x', -outerRadius - 30)
                .attr('y', -outerRadius - 30)
                .attr('rx', outerRadius + 30)
                .attr('ry', outerRadius + 30)
                .style("fill", "white")
                //.style('opacity', 0)
                //.style("fill", color(get_group(info_0.index)))
                //.transition()
                //.duration(800)
                //.style('opacity', 0)
                //.transition()
                //.duration(1)
                //.style('opacity', 1)
                //.transition()
                //.duration(800)
                //.style('opacity', 1)


            // In & Out Bar Data Process and Drawing
            leftIn = [], rightOut = [];
            leftSum = rightSum = 0;
            if (d.index % 2) {
                left = matrix_in_out[d.index - 1]
                for (var i = 0; i < left.length; ++i)
                    if (left[i] != 0) {
                        t = {}
                        t["week"] = i;
                        t["val"] = left[i];
                        leftSum += t["val"];
                        leftIn.push(t);
                    }
                right = matrix_in_out[d.index];
                for (var i = 0; i < right.length; ++i)
                    if (right[i] != 0) {
                        t = {}
                        t["week"] = i;
                        t["val"] = right[i];
                        rightSum += t["val"];
                        rightOut.push(t);
                    }
            } else {
                left = matrix_in_out[d.index]
                for (var i = 0; i < left.length; ++i)
                    if (left[i] != 0) {
                        t = {}
                        t["week"] = i;
                        t["val"] = left[i];
                        leftSum += t["val"];
                        leftIn.push(t);
                    }
                right = matrix_in_out[d.index + 1];
                for (var i = 0; i < right.length; ++i)
                    if (right[i] != 0) {
                        t = {}
                        t["week"] = i;
                        t["val"] = right[i];
                        rightSum += t["val"];
                        rightOut.push(t);
                    }
            }
            leftIn = _.sortBy(leftIn, o => o.week);
            rightOut = _.sortBy(rightOut, o => o.week);
            wholeHeight = 2 * outerRadius + 70;
            leftIn.map(i => i["height"] = Math.floor(i["val"] / leftSum * wholeHeight));
            rightOut.map(i => i["height"] = Math.floor(i["val"] / rightSum * wholeHeight));
            leftMargin = rightMargin = -outerRadius - 30
            leftIn.forEach(function(i) {
                i["y"] = leftMargin;
                leftMargin += i["height"];
            });
            rightOut.forEach(function(i) {
                i["y"] = rightMargin;
                rightMargin += i["height"];
            });

            matrix_display.selectAll(".leftBar")
                .data(leftIn)
                .enter().append("rect")
                .attr("class", "leftBar")
                .attr("x", -outerRadius - 60)
                .attr("y", d => d["y"])
                .attr("height", d => d["height"])
                .attr("width", 10)
                .style("fill", function(d, p) {
                    return color(get_group(d["week"]))
                });
            matrix_display.selectAll(".rightBar")
                .data(rightOut)
                .enter().append("rect")
                .attr("class", "rightBar")
                .attr("x", outerRadius + 50)
                .attr("y", d => d["y"])
                .attr("height", d => d["height"])
                .attr("width", 10)
                .style("fill", function(d, p) {
                    return color(get_group(d["week"]));
                });


            // Drawing Inner Unit Circle and Arcs
            var number_of_circles = matrix[index_focuses].values.length
            var content = matrix[index_focuses].values;
            var radius = 10;
            var spacing = 2 * outerRadius / (number_of_circles - 1);
            var pi = Math.PI;

            var layout_circle = matrix_display.append('g')

            var pathScale = d3.scaleLinear()
                .domain(dataService.chordScaleDomain[index_focuses])
                .range([0.1, 10]);
            arc = (from, to, value) => {
                var delta = from - to;
                value_ = pathScale(value);
                //value_ = (value / 500) > radius ? radius : _.max([value / 500, 0.2]);
                return d3.arc()
                    .innerRadius(spacing * Math.abs(delta) / 2 - +(value_))
                    .outerRadius(spacing * Math.abs(delta) / 2 + (value_))
                    .startAngle(pi / 2 + (delta < 0 ? -pi / 2 : 3 * pi / 2))
                    .endAngle(pi)()
            }
            arc_position = (from, to) => spacing * to / 2 + from * spacing / 2;

            content_bubbles = content.map(function(d, index) {
                return {
                    content: d.map(function(a, index2) {
                        return {
                            value: a,
                            parent_index: index,
                            child_index: index2
                        }
                    }),
                    index: index
                }
            });
            console.log(content_bubbles)

            var layout_circle = matrix_display.selectAll('.layout_circle')
                .data(content_bubbles)
                .enter()
                .append('g')
                .attr('opacity', 0)
            layout_circle.transition()
                .duration(800)
                .attr('opacity', 1)
                .attr('class', 'layout_circle')

            // Draw Arcs Path
            layout_circle.selectAll('path')
                .data(d => d.content)
                .enter()
                .append("path")
                .attr("d", (d, p) => p !== d.parent_index ? arc(d.parent_index, p, d.value) :
                    '')
                .attr("transform", (d, p) => "translate(" + 0 + "," + (arc_position(d.parent_index,
                    p) - outerRadius) + ")")
                //.style('fill', color(get_group(info_0.index)))
                .style("fill", function(d) {
                    if (dataService.chordPathClick.length) {
                        var clickedPath = dataService.chordPathClick;
                        if (d.parent_index == matrix[index_focuses].elements.indexOf(clickedPath[0]) &&
                            d.child_index == matrix[index_focuses].elements.indexOf(clickedPath[1]))
                            return "black";
                        else
                            return color(get_group(info_0.index));
                    } else
                        return color(get_group(info_0.index));
                })
                .style('opacity', d => d.value < threshold ? 0 : 0.6)
                .on("click", function(d) {
                    d3.event.stopPropagation();
                    dataService.getFilteredChordFromBackend("pathClick", matrix[index_focuses].elements[d.parent_index],
                        matrix[index_focuses].elements[d.child_index]);
                })
                .append("title")
                .text((d) => d.value);

            var elements_circle = matrix_display.selectAll('.layout_circle_elements')
                .data(content_bubbles)
                .enter()
                .append('g')
                .attr('opacity', 0)
            elements_circle.transition()
                .duration(800)
                .attr('opacity', 1)
                .attr('class', 'layout_circle_elements')

            var unitClick = [];
            // Draw Element Shapes
            elements_circle.append("path")
                .attr("d", d3.symbol().type(function(d, p) {
                    name = matrix[index_focuses].elements[p];
                    if (name[0] == "V")
                        return d3.symbolSquare;
                    else if (name[0] == "P")
                        return d3.symbolCircle;
                    else if (name[0] == "D")
                        return d3.symbolTriangle;
                }).size(250))
                .attr("class", "elementsUnits")
                .attr("transform", function(d, p) {
                    return "translate(0," + (p * spacing - outerRadius) + ")";
                })
                .style("fill", color(get_group(info_0.index)))
                .on("click", function(d) {
                    d3.event.stopPropagation();
                    unitClick.push(matrix[index_focuses].elements[d.index])

                    if (unitClick.length == 2) {
                        dataService.getIndividualFromBackend("chordUnit", unitClick, function(grade) {
                            drawChordGrade(grade);
                        });
                    }

                    if (unitClick.length == 1) {
                        layout_circle.selectAll("path").remove();
                        matrix_display.selectAll(".topTypes").remove();

                        layout_circle.selectAll('path')
                            .data(content_bubbles[d.index].content)
                            .enter()
                            .append("path")
                            .attr("d", (d, p) => p !== d.parent_index ? arc(d.parent_index, p, d.value) :
                                '')
                            .attr("transform", (d, p) => "translate(" + 0 + "," + (arc_position(d.parent_index,
                                p) - outerRadius) + ")")
                            .style('fill', color(get_group(info_0.index)))
                            .style('opacity', d => d.value < threshold ? 0 : 0.6);

                        leftFlag = new Array(leftIn.length).fill(-0.5);
                        rightFlag = new Array(rightOut.length).fill(-0.5);
                        leftShapeXY = [], rightShapeXY = [];
                        leftTops = _.filter(matrix[index_focuses].leftTops[d.index], i => i != "");
                        rightTops = _.filter(matrix[index_focuses].rightTops[d.index], i => i != "");
                        leftTopsVal = _.filter(matrix[index_focuses].leftTopsVal[d.index], i => i > 0);
                        rightTopsVal = _.filter(matrix[index_focuses].rightTopsVal[d.index], i => i > 0);

                        if (leftTops.length == 0 || rightTops.length == 0)
                            alert("No Data Selected");

                        matrix_display.selectAll(".leftTops").data(leftTops)
                            .enter()
                            .append("path")
                            .attr("class", "topTypes")
                            .attr("d", d3.symbol().type(function(d) {
                                if (d[0] == "V")
                                    return d3.symbolSquare;
                                else if (d[0] == "P")
                                    return d3.symbolCircle;
                                else if (d[0] == "D")
                                    return d3.symbolTriangle;
                            }).size(150))
                            .attr("transform", function(d, p) {
                                weekIdx = element_to_week[overall.indexOf(d)];
                                for (var i = 0; i < leftIn.length; ++i)
                                    if (Math.floor(leftIn[i]["week"] / 2) + 1 == weekIdx) {
                                        leftFlag[i] += 1;
                                        temp = {
                                            "x": -outerRadius - 40,
                                            "y": leftIn[i]["y"] + leftFlag[i] * 20,
                                            "colorIdx": (weekIdx - 1) * 2,
                                            "value": leftTopsVal[p],
                                            "width": pathScale(leftTopsVal[p]) * 2
                                        }
                                        leftShapeXY.push(temp);
                                        return "translate(" + temp.x + "," + temp.y + ")";
                                    }
                            })
                            .style("fill", d => color((element_to_week[overall.indexOf(d)] - 1) * 2))
                            .on("click", function(d) {
                                d3.event.stopPropagation();
                            });

                        matrix_display.selectAll(".leftTopsText").data(leftTops)
                            .enter().append("text")
                            .attr("class", "topTypes")
                            .text((d) => d)
                            .attr("transform", (d, p) => "translate(" + leftShapeXY[p].x + "," + leftShapeXY[p].y +
                                ")")
                            .style('text-anchor', 'middle')
                            .style('font-size', '5px')
                            .style("font-weight", "bold")
                            .style("fill", "white");
                        matrix_display.selectAll(".rightTops").data(rightTops)
                            .enter()
                            .append("path")
                            .attr("class", "topTypes")
                            .attr("d", d3.symbol().type(function(d) {
                                if (d[0] == "V")
                                    return d3.symbolSquare;
                                else if (d[0] == "P")
                                    return d3.symbolCircle;
                                else if (d[0] == "D")
                                    return d3.symbolTriangle;
                            }).size(150))
                            .attr("transform", function(d, p) {
                                weekIdx = element_to_week[overall.indexOf(d)];
                                for (var i = 0; i < rightOut.length; ++i)
                                    if (Math.floor(rightOut[i]["week"] / 2) + 1 == weekIdx) {
                                        rightFlag[i] += 1;
                                        temp = {
                                            "x": outerRadius + 40,
                                            "y": rightOut[i]["y"] + rightFlag[i] * 20,
                                            "colorIdx": (weekIdx - 1) * 2,
                                            "value": rightTopsVal[p],
                                            "width": pathScale(rightTopsVal[p]) * 2
                                        }
                                        rightShapeXY.push(temp);
                                        return "translate(" + temp.x + "," + temp.y + ")";
                                    }
                            })
                            .style("fill", d => color((element_to_week[overall.indexOf(d)] - 1) * 2));
                        matrix_display.selectAll(".rightTopsText").data(rightTops)
                            .enter().append("text")
                            .attr("class", "topTypes")
                            .text((d) => d)
                            .attr("transform", (d, p) => "translate(" + rightShapeXY[p].x + "," + rightShapeXY[p].y +
                                ")")
                            .style('text-anchor', 'middle')
                            .style('font-size', '5px')
                            .style("font-weight", "bold")
                            .style("fill", "white");

                        coor = d3.select(this).attr("transform");
                        coor = coor.slice(coor.indexOf("("));
                        center = [parseInt(coor.substring(1, coor.indexOf(","))), parseInt(coor.substring(coor.indexOf(
                            ",") + 1, coor.indexOf(")")))];

                        matrix_display.selectAll(".leftTopPaths").data(leftShapeXY)
                            .enter()
                            .append("path")
                            .attr("class", "topTypes")
                            .attr("d", d => "M " + (center[0] - 10) + " " + center[1] + " C " + (d.x - center[0]) / 2 +
                                " " + center[1] + ", " + (d.x - center[0]) / 2 + " " + d.y + ", " + (d.x + 10) + " " + d.y
                            )
                            .style("stroke", d => color(d["colorIdx"]))
                            .style("stroke-width", d => d["width"])
                            .style("fill", "none")
                            .append("title")
                            .text(d => "Value" + d["value"]);
                        matrix_display.selectAll(".rightTopPaths").data(rightShapeXY)
                            .enter()
                            .append("path")
                            .attr("class", "topTypes")
                            .attr("d", d => "M " + (center[0] + 10) + " " + center[1] + " C " + (d.x - center[0]) / 2 +
                                " " + center[1] + ", " + (d.x - center[0]) / 2 + " " + d.y + ", " + (d.x - 10) + " " + d.y
                            )
                            .style("stroke", d => color(d["colorIdx"]))
                            .style("stroke-width", d => d["width"])
                            .style("fill", "none")
                            .append("title")
                            .text(d => "Value: " + d["value"]);
                    }

                });

            // Draw Element Text
            elements_circle.append('text')
                .text((d, p) => matrix[index_focuses].elements[p])
                .attr('y', (d, p) => p * spacing + 2 - outerRadius)
                .attr('x', 0)
                .style('text-anchor', 'middle')
                .style('font-size', '5px')
                .style("font-weight", "bold")
                .style("fill", "white")
                .on("click", () => d3.event.stopPropagation());

        }

        function show_rect(index_focuses) {
            d3.selectAll('.matrix_display_rect')
                .transition()
                .duration(800)
                .attr('rx', 0)
                .attr('ry', 0)

            d3.selectAll('.layout_circle').transition().duration(800).style('opacity', 0).remove();
            d3.selectAll('.layout_circle_elements').transition().duration(800).style('opacity',
                0).remove();

            var matrix_rows = d3.select('.matrix_display').append('g')
            var number_of_square_per_row = matrix[index_focuses].values.length
            var content = matrix[index_focuses].values;
            var margin = 100;
            var spacing = (2 * outerRadius - 2 * margin) / (number_of_square_per_row - 1);

            rows = matrix_rows.selectAll('.row')
                .data(content)
                .enter()
                .append('g')
                .style('opacity', 0)
                .attr('transform', (d, p) => 'translate(0,' + (p - number_of_square_per_row / 2) *
                    spacing + ')')

            rows.transition()
                .duration(800)
                .style('opacity', 1)

            // Decide max value bar to be color white
            var intensity = d3.scaleLinear().domain([0, 500]).range([color(2 * index_focuses),
                'white'
            ]);
            rows.selectAll('rect')
                .data(d => d)
                .enter()
                .append('rect')
                .attr('x', (d, p) => (p - number_of_square_per_row / 2) * spacing)
                .attr('width', spacing)
                .attr('height', spacing)
                .style('fill', d => intensity(d))
                .style('stroke', 'white')
                .style('stroke-opacity', 0.2)

            rows.append('text')
                .text((d, p) => matrix[index_focuses].elements[p])
                .attr('x', -number_of_square_per_row / 2 * spacing - 6)
                .attr('y', spacing / 2 + 3)
                .style('fill', 'white')
                .attr('text-anchor', 'end')
        }


        function arcTween(newAngle) {
            return function(d) {
                var interpolate = d3.interpolate(d.endAngle, (d.startAngle + d.endAngle) / 2 + Math.PI);
                var interpolate_start_angle = d3.interpolate(d.startAngle, (d.startAngle + d.endAngle) /
                    2 - Math.PI);
                var interpolate_inner_radius = d3.interpolate(innerRadius - 20, 0.1);
                var interpolate_outer_radius = d3.interpolate(outerRadius, outerRadius + 30);
                return function(t) {
                    d.endAngle = interpolate(t);
                    d.startAngle = interpolate_start_angle(t);
                    d.innerRadius = interpolate_inner_radius(t);
                    d.outerRadius = interpolate_outer_radius(t);
                    return d3.arc()(d);
                };
            };
        }

    }


    function drawChordCtrl(d) {

        d3.select("#chordControlPanel svg").remove();
        d3.select("#chordControlPanel").style("opacity", "0");

        var ctrlMargin = {
            top: 0,
            left: 20,
            bottom: 10,
            right: 10
        };
        var element = document.getElementById("chordControl");
        var height = element.clientHeight; // height with padding
        var width = element.clientWidth; // width with padding

        var ctrlWidth = width - ctrlMargin.left - ctrlMargin.right,
            ctrlHeight = height - ctrlMargin.top - ctrlMargin.bottom;

        var svgControl = d3.select("#chordControlPanel").append("svg")
            .attr("width", ctrlWidth + ctrlMargin.left + ctrlMargin.right)
            .attr("height", ctrlHeight + ctrlMargin.top + ctrlMargin.bottom)
            .attr("transform", "translate(0,0)")
            .append("g")
            .attr("transform", "translate(" + ctrlMargin.left + "," + ctrlMargin.top + ")");

        var xDate = d3.scaleTime()
            .domain([new Date(d.time.start * 1000), new Date(d.time.end * 1000)])
            .rangeRound([0, ctrlWidth]);
        var xLinear = d3.scaleLinear()
            .domain([0, 100])
            .range([0, ctrlWidth]);
        var domainLen = [];
        for (var i = 0; i <= 10; ++i)
            domainLen.push(i * 50 + '');
        domainLen.push("500+");
        var xLen = d3.scalePoint()
            .domain(domainLen)
            .range([0, ctrlWidth]);
        var lenLinear = d3.scaleLinear().domain([0, ctrlWidth]).range([0, 550]);

        var ageCondition = [],
            dateCondition = [],
            gradeCondition = [];

        // Date Brush
        svgControl.append("g")
            .attr("class", "axis axis--grid")
            .attr("transform", "translate(0," + (ctrlHeight - 60) + ")")
            .call(d3.axisBottom(xDate)
                .ticks(d3.timeDay)
                .tickSize(-20)
                .tickFormat(function() {
                    return null;
                }));
        svgControl.append("g")
            .attr("transform", "translate(0," + (ctrlHeight - 60) + ")")
            .call(d3.axisBottom(xDate)
                .ticks(d3.timeWeek)
                .tickPadding(0)
                .tickFormat(d3.timeFormat("%b %d")))
            .attr("text-anchor", "middle");
        svgControl.append("g")
            .attr("class", "brush")
            .attr("transform", "translate(0," + (ctrlHeight - 80) + ")")
            .call(d3.brushX()
                .extent([
                    [0, 0],
                    [ctrlWidth, 20]
                ])
                .on("end", dateBrushend)
            );
        svgControl.append("text")
            .attr("transform", "translate(-20," + (ctrlHeight - 80) + ") rotate(90)")
            .attr("font-size", "10px")
            .text("Date")

        // Grade Brush
        svgControl.append("g")
            .attr("class", "axis axis--grid")
            .attr("transform", "translate(0," + (ctrlHeight - 10) + ")")
            .call(d3.axisBottom(xLinear)
                .ticks(20)
                .tickSize(-20)
                .tickFormat(function() {
                    return null;
                }));
        svgControl.append("g")
            .attr("transform", "translate(0," + (ctrlHeight - 10) + ")")
            .call(d3.axisBottom(xLinear)
                .ticks(10)
                .tickPadding(0))
            .attr("text-anchor", "middle");
        svgControl.append("g")
            .attr("class", "brush")
            .attr("transform", "translate(0," + (ctrlHeight - 30) + ")")
            .call(d3.brushX()
                .extent([
                    [0, 0],
                    [ctrlWidth, 20]
                ])
                .on("end", gradeBrushend)
            );
        svgControl.append("text")
            .attr("transform", "translate(-20," + (ctrlHeight - 30) + ") rotate(90)")
            .attr("font-size", "10px")
            .text("Grade")


        function gradeBrushend() {
            if (!d3.event.sourceEvent) return;
            if (!d3.event.selection) return;
            gradeCondition = d3.brushSelection(this).map(xLinear.invert).map(i => Math.round(i));

            console.log("Chord Grade Selected");
            dataService.getFilteredChordFromBackend("filterBrush", dateCondition, gradeCondition);
        }

        function dateBrushend() {
            if (!d3.event.sourceEvent) return;
            if (!d3.event.selection) return;
            var d0 = d3.event.selection.map(xDate.invert),
                d1 = d0.map(d3.timeDay.round);
            // If empty when rounded, use floor & ceil instead.
            if (d1[0] >= d1[1]) {
                d1[0] = d3.timeDay.floor(d0[0]);
                d1[1] = d3.timeDay.offset(d1[0]);
            }
            d3.select(this).transition().call(d3.event.target.move, d1.map(xDate));
            dateCondition = d1.map(i => Date.parse(i) / 1000);

            console.log("Chord Date selected");
            dataService.getFilteredChordFromBackend("filterBrush", dateCondition, gradeCondition);
        }

    }

    function drawChordGrade(d) {
        d3.select("#chordControlGrade svg").remove();
        d3.select("#chordControlGrade").style("opacity", "1");
        d3.select("#chordControlPanel").style("opacity", "0");
        showPanel = true;

        var xVal = Array(101).fill(0);
        d.map(i => xVal[i] += 1);
        data = [];
        for (var i = 0; i <= 100; ++i) {
            t = {}
            t["x"] = i + '';
            t["val"] = xVal[i];
            data.push(t);
        }

        var ctrlMargin = {
            top: 10,
            left: 30,
            bottom: 20,
            right: 10
        };

        var element = document.getElementById("chordControl");
        var computedStyle = getComputedStyle(element);
        var height = element.clientHeight; // height with padding
        var width = element.clientWidth; // width with padding

        var ctrlWidth = width - ctrlMargin.left - ctrlMargin.right,
            ctrlHeight = height - ctrlMargin.top - ctrlMargin.bottom;

        var svgControlGrade = d3.select("#chordControlGrade").append("svg")
            .attr("width", ctrlWidth + ctrlMargin.left + ctrlMargin.right)
            .attr("height", ctrlHeight + ctrlMargin.top + ctrlMargin.bottom)
            .attr("transform", "translate(0,0)")
            .append("g")
            .attr("transform", "translate(" + ctrlMargin.left + "," + ctrlMargin.top + ")");
        var chordTooltip = d3.select("#chordControlGrade").append("div").attr("class", "chordTooltip");

        var x = d3.scaleBand().domain(data.map(d => d.x)).rangeRound([0, ctrlWidth]).padding(0.1),
            y = d3.scaleLinear().domain([0, d3.max(data, d => d.val)]).rangeRound([ctrlHeight, 0]);

        svgControlGrade.append("g")
            .attr("class", "axis axis--x")
            .attr("transform", "translate(0," + ctrlHeight + ")")
            .call(d3.axisBottom(x).tickValues(x.domain().filter((d, i) => !(i % 5))));

        svgControlGrade.append("g")
            .attr("class", "axis axis--y")
            .call(d3.axisLeft(y).ticks(5))
            .append("text")
            .attr("transform", "rotate(-90)")
            .attr("y", 6)
            .attr("dy", "0.71em")
            .attr("text-anchor", "end")
            .attr("fill", "#5D6971");

        svgControlGrade.selectAll(".bar")
            .data(data)
            .enter().append("rect")
            .attr("x", d => x(d.x))
            .attr("y", d => y(d.val))
            .attr("width", x.bandwidth())
            .attr("height", d => ctrlHeight - y(d.val))
            .attr("fill", "#9ecae1")
            .on("mousemove", function(d) {
                chordTooltip
                    .style("left", d3.select(this).attr("x") + 'px')
                    .style("top", (d3.select(this).attr("y") - 60) + 'px')
                    .style("display", "inline-block")
                    .html("Grade: " + (d.x) + "<br>" + "Sum: " + (d.val));
            })
            .on("mouseout", function(d) {
                chordTooltip.style("display", "none");
            });


    }

}