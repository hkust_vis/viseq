let drawScatter = function(raw) {

	var showPanel = true;
	$("#scatter-filter-brush").click(function() {
		console.log('Filter Scatter Event')
		if (showPanel) {
			d3.select("#scatterControl")
				.transition()
				.duration(600)
				.style("opacity", "1");
			showPanel = false;
		} else {
			d3.select("#scatterControl")
				.transition()
				.duration(600)
				.style("opacity", "0");
			showPanel = true;
		}
	});

	// Draw Students >=60 when enter
	data60 = [];
	raw.forEach(function(i) {
		if (i["grade"] >= 60)
			data60.push(i)
	});
	drawScatterPoints(data60);
	drawScatterCtrl();


	function drawScatterPoints(data) {

		d3.select("#scatterSvg svg").remove();

		var margin = {
			top: 0,
			right: 10,
			bottom: 10,
			left: 10
		};
		var element = document.getElementById("scatterSvg");
		var height = element.clientHeight; // height with padding
		var width = element.clientWidth; // width with padding

		width -= margin.left + margin.right;
		height -= margin.top + margin.bottom;

		var xValue = function(d) {
				return d.x[0];
			},
			xScale = d3.scaleLinear()
			.domain([d3.min(data, function(d) {
				return d.x[0]
			}) - 2, d3.max(data, function(d) {
				return d.x[0]
			}) + 2])
			.range([0, width]),
			xMap = function(d) {
				return xScale(xValue(d));
			};

		var yValue = function(d) {
				return d.y[0];
			},
			yScale = d3.scaleLinear()
			.domain([d3.min(data, function(d) {
				return d.y[0]
			}) - 1, d3.max(data, function(d) {
				return d.y[0]
			}) + 1])
			.range([height, 0]),
			yMap = function(d) {
				return yScale(yValue(d));
			};

		var svgScatter = d3.select("#scatterSvg").append("svg")
			.attr("width", width + margin.left + margin.right)
			.attr("height", height + margin.top + margin.bottom)
			.append("g")
			.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

		var tooltip = d3.select("#scatterSvg").append("div")
			.attr("class", "tooltip")
			.style("opacity", 0)

		svgScatter.selectAll(".dot")
			.data(data)
			.enter().append("circle")
			.attr("class", "dot")
			.attr("r", 3.5)
			.attr("cx", xMap)
			.attr("cy", yMap)
			.style("opacity", "0.65")
			.style("fill", "grey")
			.on("mouseover", function(d) {
				tooltip.transition()
					.duration(200)
					.style("opacity", 1);
				tooltip.html(d.userId + "<br/> Grade:" + d.grade)
					.style("left", (d3.event.pageX + 5) + "px")
					.style("top", d3.event.pageY / 2 + "px");
			})
			.on("mouseout", function(d) {
				tooltip.transition()
					.duration(200)
					.style("opacity", 0);
			});



		var scatterBrush = d3.brush()
			.extent([
				[0, 0],
				[width + margin.left + margin.right, height + margin.bottom + margin.top]
			])
			.on("end", function() {
				var selectedId = [];
				var topX = d3.select(".selection").node().x.baseVal.value;
				var topY = d3.select(".selection").node().y.baseVal.value;
				var brushWidth = d3.select(".selection").node().width.baseVal.value;
				var brushHeight = d3.select(".selection").node().height.baseVal.value;
				var extent = [
					[topX, topX + brushWidth],
					[topY, topY + brushHeight]
				]
				d3.selectAll("#scatterSvg .dot").each(function(d) {
					if (extent[0][0] <= xMap(d) && xMap(d) <= extent[0][1] &&
						extent[1][0] <= yMap(d) && yMap(d) <= extent[1][1])
						selectedId.push(d.userId);
				});
				dataService.getIndividualFromBackend("scatterBrush", selectedId);
				dataService.getFilteredChordFromBackend("scatterBrush", selectedId, '');
				drawSpmf(dataService.courseInfo, dataService.patternData);
			});
		svgScatter.append("g")
			.attr("class", "brush")
			.call(scatterBrush);
	}

	function drawScatterCtrl() {

		d3.select("#scatterControl svg").remove();
		d3.select("#scatterControl").style("opacity", "0");

		var ctrlMargin = {
			top: 0,
			left: 20,
			bottom: 10,
			right: 10
		};

		var element = document.getElementById("scatterControl");
		var height = element.clientHeight; // height with padding
		var width = element.clientWidth; // width with padding

		var ctrlWidth = width - ctrlMargin.left - ctrlMargin.right,
			ctrlHeight = height - ctrlMargin.top - ctrlMargin.bottom;

		var svgControl = d3.select("#scatterControl").append("svg")
			.attr("width", ctrlWidth + ctrlMargin.left + ctrlMargin.right)
			.attr("height", ctrlHeight + ctrlMargin.top + ctrlMargin.bottom)
			.attr("transform", "translate(0,0)")
			.append("g")
			.attr("transform", "translate(" + ctrlMargin.left + "," + ctrlMargin.top + ")");

		var xLinear = d3.scaleLinear()
			.domain([0, 100])
			.range([0, ctrlWidth]);

		// Grade Brush
		svgControl.append("g")
			.attr("class", "axis axis--grid")
			.attr("transform", "translate(0," + (ctrlHeight - 10) + ")")
			.call(d3.axisBottom(xLinear)
				.ticks(20)
				.tickSize(-10)
				.tickFormat(function() {
					return null;
				}));
		svgControl.append("g")
			.attr("transform", "translate(0," + (ctrlHeight - 10) + ")")
			.call(d3.axisBottom(xLinear)
				.ticks(10)
				.tickPadding(0))
			.attr("text-anchor", "middle");
		svgControl.append("g")
			.attr("class", "brush")
			.attr("transform", "translate(0," + (ctrlHeight - 20) + ")")
			.call(d3.brushX()
				.extent([
					[0, 0],
					[ctrlWidth, 10]
				])
				.on("end", gradeBrushend)
			);
		svgControl.append("text")
			.attr("transform", "translate(-10," + (ctrlHeight - 20) + ") rotate(90)")
			.attr("font-size", "10px")
			.text("Grade")

		var gradeCondition = [];

		function gradeBrushend() {
			if (!d3.event.sourceEvent) return;
			if (!d3.event.selection) return;
			gradeCondition = d3.brushSelection(this).map(xLinear.invert).map(i => Math.round(i));

			console.log("Scatter Grade Selected");
			var filteredData = [];
			raw.forEach(function(i) {
				if (i["grade"] >= gradeCondition[0] && i["grade"] <= gradeCondition[1])
					filteredData.push(i)
			});
			drawScatterPoints(filteredData);
			dataService.getIndividualFromBackend("scatterGrade", gradeCondition);
			//dataService.getFilteredChordFromBackend("brush", dateCondition, gradeCondition);
		}

	}



}
