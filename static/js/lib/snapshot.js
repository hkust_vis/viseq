document.onpaste = function(event){
  var items = (event.clipboardData || event.originalEvent.clipboardData).items;
  console.log(JSON.stringify(items)); // will give you the mime types
  for (index in items) {
    var item = items[index];
    if (item.kind === 'file') {
      var blob = item.getAsFile();
      var reader = new FileReader();
      reader.onload = function(event){
        console.log(event.target.result)
        var template = $('#snapshot-template').html();


// var date = new Date;
// date.setTime(Date.now());

// var seconds = date.getSeconds();
// var minutes = date.getMinutes();
// var hour = date.getHours();

// var year = date.getFullYear();
// var month = date.getMonth(); // beware: January = 0; February = 1, etc.
// var day = date.getDate();

// var dayOfWeek = date.getDay(); // Sunday = 0, Monday = 1, etc.
// var milliSeconds = date.getMilliseconds();

        var time = moment().format('LTS')

        template = template.replace("SCREENSHOT-NAME","SCREENSHOT - "+time)
        template = template.replace("SCREENSHOT-CONTENT",'<img src="'+event.target.result+'" />')
        console.log(template)

        // $('#screenshots').append('<img src="'+event.target.result+'" style="width:100%; border: 1px solid black; margin: 10px;"/>')
        $('#screenshots').append(template)


        var switchToInput = function() {
          var $input = $("<input>", {
            val: $(this).text(),
            type: "text"
          });
          $input.addClass("loadNum");
          $(this).replaceWith($input);
          $input.on("blur", switchToSpan);
          $input.select();
        };
        var switchToSpan = function() {
          var $span = $("<span>", {
            text: $(this).val()
          });
          $span.addClass("loadNum");
          $(this).replaceWith($span);
          $span.on("click", switchToInput);
        }
        $(".screenshot-name").on("click", switchToInput);





        }; // data url!

      reader.readAsDataURL(blob);
    }
  }
}
