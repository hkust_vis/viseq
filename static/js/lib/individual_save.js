let drawIndividual = function(individualData, courseInfo) {
    console.log('individual ', individualData.length);
    d3.select("#individualTitle").style("display", "block");
    d3.select("#individualSvg svg").remove();
    d3.select("#individualSvg p").remove();
    d3.select("#individualControl svg").remove();
    let simSign = false;
    var showPanel = true;

    $("#individualFilter").click(function() {
        if (showPanel) {
            d3.select("#individualControl")
                .transition()
                .duration(600)
                .style("opacity", "1");
            showPanel = false;
        } else {
            d3.select("#individualControl")
                .transition()
                .duration(600)
                .style("opacity", "0");
            showPanel = true;
        }
    });
    var individualType;

    $("#ballonView").click(function() {
        individualType = "ballon";
        drawIndividualLines(individualData, individualType);
        drawIndividualCtrl(courseInfo);
    });

    $("#chainView").click(function() {
        individualType = "chain";
        drawIndividualLines(individualData, individualType);
        drawIndividualCtrl(courseInfo);
    });


    let idIndividualMap = {};
    individualData.forEach(function(d){
        idIndividualMap[d['userId']] = d;
    });


    function findList(queryResult){
        queryResult.sort(function(a, b){
            return a['val'] - b['val'];
        });
        let tempArr = [];
        queryResult.forEach(function(result, i){

            let id = result['userId'];
            let val = result['val'];
            if(idIndividualMap[id] != undefined){
                console.log('tell', id, val, result)
                idIndividualMap[id]['individualDistance'] = val;
                tempArr.push(idIndividualMap[id])
            }
        });
        return tempArr;
    };

    function drawIndividualLines(data, type) {

        d3.select("#individualSvg svg").remove();
        d3.select("#individualSvg p").remove();
        d3.select("#individualLegend svg").remove();
        d3.select("#individualLegend text").remove();

        if (dataService.courseName == "2014") {
            var overall = ['V1.1', 'V1.2', 'V1.3', 'D1', 'V1.4', 'V1.5', 'V1.6', 'V1.7', 'V1.8', 'V1.9',
                'V1.10', 'V1.11', 'V1.12', 'PW01.1', 'PW01.2', 'V1.13', 'V1.14', 'PL01.1', 'V2.1', 'V2.2',
                'V2.3', 'PW02.1', 'V2.4', 'PW02.2', 'V2.5', 'V2.6', 'V2.7', 'PW02.3', 'V2.8', 'V2.9', 'V2.10',
                'PW02.4', 'D2', 'V2.11', 'V2.12', 'PL02.1', 'PL02.2', 'PL02.3', 'V3.1', 'V3.2', 'V3.3',
                'V3.4',
                'V3.5', 'V3.6', 'PW03.1', 'D3.1', 'V3.7', 'D3.2', 'V3.8', 'V3.9', 'V3.10', 'V3.11', 'V3.12',
                'PW03.2', 'V3.13', 'V3.14', 'V3.15', 'PW03.3', 'PW03.4', 'PL03.1', 'PL03.2', 'PL03.3',
                'PL03.4', 'V4.1', 'V4.2', 'V4.3', 'PW04.1', 'V4.4', 'V4.5', 'V4.6', 'V4.7', 'D4', 'V4.8',
                'PW04.2', 'V4.9', 'V4.10', 'V4.11', 'V4.12', 'PW04.3', 'PW04.4', 'V5.1', 'V5.2', 'V5.3',
                'PW05.1', 'V5.4', 'V5.5', 'V5.6', 'PW05.2', 'PW05.3', 'V5.7', 'PW05.4', 'PW05.5', 'V5.8',
                'D5',
                'V5.9', 'V5.10', 'V5.11', 'V5.12', 'V5.13', 'PL04.1', 'PL04.2', 'PL04.3', 'PL04.4', 'V6.1',
                'V6.2', 'PW06.1', 'V6.3', 'PW06.2', 'D6', 'V6.4', 'PW06.3', 'V6.5', 'V6.6', 'V6.7', 'PW06.4',
                'V6.8', 'PW06.5', 'V6.9', 'PW06.6', 'V6.10', 'V6.11', 'V7.1', 'V7.2', 'V7.3', 'PW07.1',
                'V7.4',
                'PW07.2', 'V7.5', 'PW07.3', 'V7.6', 'V7.7', 'V7.8', 'V7.9', 'PW07.4', 'V7.10', 'V7.11',
                'PL05.1', 'PL05.2', 'PL05.3', 'PL05.4', 'V8.1', 'V8.2', 'V8.3', 'PW08.1', 'V8.4', 'V8.5',
                'PW08.2', 'V8.6', 'V8.7', 'V8.8', 'V8.9', 'V8.10', 'V8.11', 'PW08.3', 'PL06.1', 'PL06.2',
                'PL06.3', 'PL06.4', 'PL06.5', 'V9.1', 'V9.2', 'V9.3', 'PW09.1', 'V9.4', 'PW09.2', 'V9.5',
                'PW09.3', 'D9', 'V9.6', 'V9.7', 'V9.8', 'V9.9', 'V9.10', 'V9.11', 'PW09.4', 'V10.1', 'V10.2',
                'V10.3', 'V10.4', 'PW10.1', 'V10.5', 'V10.6', 'V10.7', 'V10.8', 'V10.9', 'PW10.2', 'V10.10',
                'V10.11', 'V10.12', 'PF01', 'PF02', 'PF03', 'PF04', 'PF05', 'PF06', 'PF07', 'PF08', 'PF09',
                'PF10', 'PF11', 'PF12', 'PF13', 'PF14', 'PF15', 'PF16', 'PF17', 'PF18', 'PF19', 'PF20',
                'PF21',
                'PF22', 'PF23', 'PF24', 'PF25', 'PF26', 'PF27', 'PF28', 'PF29', 'PF30'
            ];
            var element_to_week = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2,
                2,
                2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
                3,
                3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5, 5, 5, 5,
                5,
                5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,
                6,
                7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
                8,
                8, 8, 8, 8, 8, 8, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 10, 10, 10, 10, 10, 10, 10,
                10, 10, 10, 10, 10, 10, 10, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11,
                11,
                11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11
            ];
            var colorDomain = [0, 11];
            var colorRange = ['#9e0142', '#d53e4f', '#f46d43', '#fdae61', '#fee08b', '#e6f598', '#abdda4',
                '#66c2a5',
                '#3288bd', '#5e4fa2', "#7D828A"
            ];
        } else if (dataService.courseName == "2015") {
            var overall = ['V1.1', 'V1.2', 'V1.3', 'V1.4', 'V1.5', 'V1.6', 'V1.7', 'V1.8', 'V1.9', 'V1.10',
                'V1.11', 'PW01.1', 'PW01.2', 'V1.12', 'V1.13', 'PL01.1', 'V2.1', 'V2.2', 'V2.3', 'PW02.1',
                'V2.4', 'PW02.2', 'V2.5', 'V2.6', 'V2.7', 'PW02.3', 'V2.8', 'V2.9', 'V2.10', 'PW02.4', 'D2',
                'V2.11', 'V2.12', 'PL02.1', 'PL02.2', 'PL02.3', 'V3.1', 'V3.2', 'V3.3', 'V3.4', 'V3.5',
                'V3.6',
                'PW03.1', 'D3.1', 'V3.7', 'D3.2', 'V3.8', 'V3.9', 'V3.10', 'V3.11', 'V3.12', 'PW03.2',
                'V3.13',
                'V3.14', 'V3.15', 'PW03.3', 'PW03.4', 'PL03.1', 'PL03.2', 'PL03.3', 'PL03.4', 'V4.1', 'V4.2',
                'V4.3', 'PW04.1', 'V4.4', 'V4.5', 'V4.6', 'V4.7', 'D4', 'V4.8', 'V4.9', 'PW04.2', 'V4.10',
                'V4.11', 'V4.12', 'PW04.3', 'PW04.4', 'V4.13', 'PL04.1', 'PL04.2', 'PL04.3', 'V5.1', 'V5.2',
                'V5.3', 'PW05.1', 'V5.4', 'V5.5', 'V5.6', 'PW05.2', 'PW05.3', 'V5.7', 'PW05.4', 'PW05.5',
                'V5.8', 'D5.1', 'V5.8', 'V5.9', 'PW05.6', 'V5.10', 'PW05.7', 'D5.2', 'V5.11', 'PW05.8',
                'PL05.1', 'PL05.2', 'PF01', 'PF02', 'PF03', 'PF04', 'PF05', 'PF06', 'PF07', 'PF08', 'PF09',
                'PF10', 'PF11a', 'PF11b', 'PF11c', 'PF11d', 'PF11e', 'PF12A', 'PF12B', 'PF12C', 'PF12D',
                'PF12E', 'PF12F', 'PF12G', 'PF12H', 'PF12I', 'PF12J', 'PF13A', 'PF13B', 'PF13C', 'PF13D',
                'PF14', 'PF15A', 'PF15B'
            ];
            var element_to_week = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2,
                2,
                2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
                3,
                3, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5, 5,
                5,
                5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,
                6,
                6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6
            ];
            var colorDomain = [0, 6];
            var colorRange = ["#d7191c", "#fdae61", "#ffffbf", "#abdda4", "#2b83ba", "#7D828A"];
        }
        var overall = ['V1.1', 'V1.2', 'V1.3', 'D1', 'V1.4', 'V1.5', 'V1.6', 'V1.7', 'V1.8', 'V1.9',
            'V1.10', 'V1.11', 'V1.12', 'PW01.1', 'PW01.2', 'V1.13', 'V1.14', 'PL01.1', 'V2.1', 'V2.2',
            'V2.3', 'PW02.1', 'V2.4', 'PW02.2', 'V2.5', 'V2.6', 'V2.7', 'PW02.3', 'V2.8', 'V2.9', 'V2.10',
            'PW02.4', 'D2', 'V2.11', 'V2.12', 'PL02.1', 'PL02.2', 'PL02.3', 'V3.1', 'V3.2', 'V3.3', 'V3.4',
            'V3.5', 'V3.6', 'PW03.1', 'D3.1', 'V3.7', 'D3.2', 'V3.8', 'V3.9', 'V3.10', 'V3.11', 'V3.12',
            'PW03.2', 'V3.13', 'V3.14', 'V3.15', 'PW03.3', 'PW03.4', 'PL03.1', 'PL03.2', 'PL03.3',
            'PL03.4',
            'V4.1', 'V4.2', 'V4.3', 'PW04.1', 'V4.4', 'V4.5', 'V4.6', 'V4.7', 'D4', 'V4.8', 'PW04.2',
            'V4.9', 'V4.10', 'V4.11', 'V4.12', 'PW04.3', 'PW04.4', 'V5.1', 'V5.2', 'V5.3', 'PW05.1',
            'V5.4',
            'V5.5', 'V5.6', 'PW05.2', 'PW05.3', 'V5.7', 'PW05.4', 'PW05.5', 'V5.8', 'D5', 'V5.9', 'V5.10',
            'V5.11', 'V5.12', 'V5.13', 'PL04.1', 'PL04.2', 'PL04.3', 'PL04.4', 'V6.1', 'V6.2', 'PW06.1',
            'V6.3', 'PW06.2', 'D6', 'V6.4', 'PW06.3', 'V6.5', 'V6.6', 'V6.7', 'PW06.4', 'V6.8', 'PW06.5',
            'V6.9', 'PW06.6', 'V6.10', 'V6.11', 'V7.1', 'V7.2', 'V7.3', 'PW07.1', 'V7.4', 'PW07.2', 'V7.5',
            'PW07.3', 'V7.6', 'V7.7', 'V7.8', 'V7.9', 'PW07.4', 'V7.10', 'V7.11', 'PL05.1', 'PL05.2',
            'PL05.3', 'PL05.4', 'V8.1', 'V8.2', 'V8.3', 'PW08.1', 'V8.4', 'V8.5', 'PW08.2', 'V8.6', 'V8.7',
            'V8.8', 'V8.9', 'V8.10', 'V8.11', 'PW08.3', 'PL06.1', 'PL06.2', 'PL06.3', 'PL06.4', 'PL06.5',
            'V9.1', 'V9.2', 'V9.3', 'PW09.1', 'V9.4', 'PW09.2', 'V9.5', 'PW09.3', 'D9', 'V9.6', 'V9.7',
            'V9.8', 'V9.9', 'V9.10', 'V9.11', 'PW09.4', 'V10.1', 'V10.2', 'V10.3', 'V10.4', 'PW10.1',
            'V10.5', 'V10.6', 'V10.7', 'V10.8', 'V10.9', 'PW10.2', 'V10.10', 'V10.11', 'V10.12', 'PF01',
            'PF02', 'PF03', 'PF04', 'PF05', 'PF06', 'PF07', 'PF08', 'PF09', 'PF10', 'PF11', 'PF12', 'PF13',
            'PF14', 'PF15', 'PF16', 'PF17', 'PF18', 'PF19', 'PF20', 'PF21', 'PF22', 'PF23', 'PF24', 'PF25',
            'PF26', 'PF27', 'PF28', 'PF29', 'PF30'
        ];


        //var subdata = data.filter(a => a.distance.length < 500);
        if (data.length > 50)
            var subdata = data.slice(0, 50)
        else {
            var subdata = data;
            if (subdata.length == 0) {
                d3.select("#individualSvg").append("p")
                    .style("text-align", "center")
                    .style("font-size", "30px")
                    .text("No Coresponding Data Selected");
                return;
            }
        }

        var maxLen = _.max(subdata, i => i["sequence"].length);
        var viewMargin = {
                top: 20,
                right: 10,
                bottom: 10,
                left: 0
            },
            viewlWidth = maxLen["sequence"].length * 50,
            viewHeight = subdata.length * 280 + viewMargin.bottom;
        var svgView = d3.select("#individualSvg").append("svg")
            .attr("width", viewlWidth + viewMargin.left)
            .attr("height", viewHeight + viewMargin.top)
            .append("g")
            .attr("transform", "translate(" + viewMargin.left + "," + viewMargin.top + ")");

        var color_week = d3.scaleQuantize()
            .domain(colorDomain)
            .range(colorRange);


        if (type == "chain") {
            let groupPerLen = 3;
            let individualPerLen = 25;
            let eleGap = 3;

            var generateData = function(d) {
                let _studentsRowData = d['studentsRowData'];

                let _studentLineData = [];
                let _gapQ = 25;
                let offsetX = 0;

                _studentsRowData.forEach(function(d, i) {
                    if (i == 0) {
                        if (d['numOfUnfold'] == 2) {
                            offsetX = individualPerLen / 2;
                        } else if (d['numOfUnfold'] == 1) {
                            offsetX = individualPerLen / 2;
                        } else {
                            offsetX = groupPerLen / 2;
                        }
                    }
                    var flag = 0;
                    if (d['prev'].includes('PF')) flag += 1;
                    if (d["last"].includes("PF")) flag += 1;
                    var gap = distance(d.dis) * 10;

                    let _dv = 0;

                    if (d['numOfUnfold'] == 1) {
                        _dv = (individualPerLen + groupPerLen) / 2
                    } else if (d['numOfUnfold'] == 2) {
                        _dv = individualPerLen
                    } else {
                        _dv = groupPerLen
                    }
                    if (d['segStatus'] == 'inter') {
                        _dv += eleGap;
                    }

                    if (flag == 2) {
                        _studentLineData.push({
                            x: offsetX,
                            y: 0
                        });
                        _studentLineData.push({
                            x: offsetX,
                            y: gap
                        });
                        offsetX += _dv;
                        _studentLineData.push({
                            x: offsetX,
                            y: gap
                        });
                        _studentLineData.push({
                            x: offsetX,
                            y: 0
                        });
                    } else if (flag == 1) {
                        let _dy = overall.indexOf(d["prev"]) < overall.indexOf(d["last"]) ? 70 : 70
                        _studentLineData.push({
                            x: offsetX,
                            y: 0
                        });
                        _studentLineData.push({
                            x: offsetX,
                            y: _dy
                        });
                        offsetX += _dv;
                        _studentLineData.push({
                            x: offsetX,
                            y: _dy
                        });
                        _studentLineData.push({
                            x: offsetX,
                            y: 0
                        });
                    } else {

                        _studentLineData.push({
                            x: offsetX,
                            y: 0
                        });
                        offsetX += _dv / 2;
                        _studentLineData.push({
                            x: offsetX,
                            y: distance(d.dis) * 5
                        });
                        offsetX += _dv / 2;
                        _studentLineData.push({
                            x: offsetX,
                            y: 0
                        });
                    }
                });
                return _studentLineData;
            }

            student_layout_rows = svgView.selectAll('.student')
                .data(subdata)
                .enter()
                .append('g')
                .attr('transform', (d, p) => 'translate(0,' + (p * 150 + 80) + ')');

            let distance = n => (n - 1) >= 0 ? -Math.sqrt(n - 1) : Math.sqrt(1 - n);
            var line = d3.line()
                .x(function(d) {
                    return d['x'];
                })
                .y(function(d) {
                    return d['y'];
                });
            subdata.forEach(function(segObj) {
                let sequence = segObj['sequence'];
                let _tempSeg = null;
                let segSequence = [];
                let currentType = null;
                for (let i = 0, ilen = sequence.length; i < ilen; i++) {
                    let ele = sequence[i];
                    let _currentType = element_to_week[overall.indexOf(ele)];
                    if (i == 0) {
                        _tempSeg = {
                            'segs': [],
                            'type': _currentType,
                            'status': 'fold'
                        }; //fold
                        _tempSeg.segs.push(ele);
                        currentType = _currentType
                    } else if (_currentType != currentType) {
                        let _testType = null
                        // if(i % 2 == 0) _testType = 'unfold';
                        // else _testType = 'fold';
                        segSequence.push(_tempSeg);
                        _tempSeg = {
                            'segs': [],
                            'type': _currentType,
                            'status': 'fold'
                        };
                        _tempSeg.segs.push(ele);
                        currentType = _currentType;
                    } else {
                        _tempSeg.segs.push(ele);
                    }
                }
                segObj['segSequence'] = segSequence;
            });
            //console.log('raw', subdata)

            let updateRowPathData = function(rowObj) {
                let segObjSenquence = rowObj['segSequence']
                let _studentsRowData = [];
                let index = 0;

                for (var i = 0, ilen = segObjSenquence.length; i < ilen; i++) {
                    let segs = segObjSenquence[i]['segs'];
                    for (var j = 0, jlen = segs.length; j < jlen; j++) {
                        let t = {
                            'prev': segs[j],
                            'dis': rowObj['distance'][index]
                        };

                        if (j != jlen - 1) {
                            t['last'] = segs[j + 1];
                            t['numOfUnfold'] = segObjSenquence[i]['status'] == 'unfold' ? 2 : 0;
                            t['seqStatus'] = 'inner'
                        } else {
                            if (i + 1 == segObjSenquence.length) break
                            t['last'] = segObjSenquence[i + 1]['segs'][0];
                            t['segStatus'] = 'inter';
                            if (segObjSenquence[i]['status'] == 'unfold' && segObjSenquence[i + 1]['status'] ==
                                'unfold') {
                                t['numOfUnfold'] = 2;
                            } else if (segObjSenquence[i]['status'] == 'fold' && segObjSenquence[i + 1]['status'] ==
                                'fold') {
                                t['numOfUnfold'] = 0;
                            } else {
                                t['numOfUnfold'] = 1;
                            }
                        }
                        _studentsRowData.push(t);
                        index += 1;
                        if (index == rowObj.sequence.length) break
                    }
                }
                rowObj['studentsRowData'] = _studentsRowData;
            };

            subdata.forEach(function(rowObj) {
                updateRowPathData(rowObj);
            });



            let drawRowPath = function(e, d) {
                let _studetnLineData = generateData(d);
                d3.select(e).selectAll('.lineContainer').remove();
                let _container = d3.select(e).append('g').attr('class', 'lineContainer')
                let row_pannel = _container.append('g').attr('transform','translate(50,0)');

                row_pannel
                    .datum(_studetnLineData)
                    .append('path')
                    .attr('class', (d, i) => 'path' + i)
                    .style("fill", "none")
                    .attr('d', line)
                    .attr('stroke-width', 1)
                    .attr('stroke', '#777')
                    .attr('opacity', 0.8)
            }
            let drawRow = function(e, d) {

                let _this = e;
                d3.select(e).selectAll('.sequenceElements').remove();
                let _rowContnainer = d3.select(e).append('g').attr('class', 'sequenceElements');
                let circle = _rowContnainer.append('circle').attr('cx', 5).attr('r', 10).attr('fill', 'red');
                _rowContnainer.append('text').text(function(d){
                    return d['individualDistance']
                })
                circle.on('click', function(d){
                    dataService.getIndividualSimilarityFromBackend(d['userId'], function(queryResult){
                        simSign = true;
                        let newList = findList(queryResult);
                        drawIndividualLines(newList, 'chain')
                    })
                })


                let _sequenceContaienr = _rowContnainer.append('g')
                    .attr('transform','translate(50,0)');
                let offsetX = 0;

                let sequenceSegContainer = _sequenceContaienr.selectAll('.segContainer').data(d.segSequence)
                    .enter()
                    .append("g").attr('class', 'segContainer')
                    .attr("transform", function(segsObj, p) {
                        let _seq = segsObj.segs;
                        if (segsObj.status == 'fold') {
                            let _offx = offsetX;
                            offsetX += (eleGap + _seq.length * groupPerLen)
                            return "translate(" + _offx + ",0)";
                        } else if (segsObj.status == 'unfold') {
                            let _offx = offsetX;
                            offsetX += (eleGap + _seq.length * individualPerLen)
                            return "translate(" + _offx + ",0)";
                        }

                    });
                // sequenceSegContainer.append('rect').attr('width', function(segsObj){
                //     if(segsObj['status'] == 'unfold'){
                //         return segsObj['segs'].length * individualPerLen;
                //     }else{
                //
                //         return segsObj['segs'].length * groupPerLen;
                //
                //     }
                // })
                //     .attr('height', 50)
                //     .attr('fill', 'red')
                //     .attr('opacity', 0.2)
                //     .attr('stroke','black')



                sequenceSegContainer.each(function(segsObj) {
                    let className = 'seg-container-' + segsObj[status];
                    let _container = d3.select(this).append('g').attr('class', className);

                    if (segsObj['status'] == 'fold') {
                        _container.append('rect').attr('width', segsObj.segs.length * groupPerLen)
                            .attr('height', 2)
                            .attr('fill', function(segsObj) {
                                return color_week(segsObj['type'] - 1);
                            })
                            .attr('stroke', function(segsObj) {
                                return color_week(segsObj['type'] - 1);
                            })
                            .attr('y', -2.5)
                            .on('click', function() {
                                segsObj['status'] = 'unfold';
                                updateRowPathData(d);
                                drawRowPath(_this, d);
                                drawRow(_this, d);

                            })
                    } else {
                        let shapeContainers = _container.selectAll('.shape')
                            .data(segsObj.segs)
                            .enter()
                            .append('g')
                            .attr('class', 'shape')
                            .attr('transform', function(d, i) {
                                let x = i * individualPerLen + 11;
                                return "translate(" + x + ',' + 0 + ')';
                            });

                        shapeContainers.append('path')
                            .attr('d', d3.symbol().type(function(d) {
                                if (d[0] == "V")
                                    return d3.symbolSquare;
                                else if (d[0] == "P")
                                    return d3.symbolCircle;
                                else if (d[0] == "D")
                                    return d3.symbolTriangle;
                            }).size(250))
                            .attr("fill", d => color_week(element_to_week[overall.indexOf(d)] - 1))
                            .style("opacity", "0.7")
                            .style("stroke", function(d) {
                                if (d[0] == "P")
                                    return "black";
                            })
                            .style("stroke-width", function(d) {
                                if (d[0] == "P")
                                    return "2px";
                            })
                            .on('click', function() {
                                segsObj['status'] = 'fold'

                                updateRowPathData(d);
                                drawRowPath(_this, d);
                                drawRow(_this, d);
                            })

                        shapeContainers.append('text')
                            .attr('x', 0)
                            .text(d => d)
                            .attr('y', 2)
                            .attr("font-size", "8px")
                            .attr("font-weight", "bold")
                            .attr("fill", "#000")
                            .attr("text-anchor", "middle")
                            .style('point-events', 'none')
                            .style('cursor', 'none')
                    }
                });


            }

            student_layout_rows.each(function(d) {
                d['pathContainer'] = this;
                d3.select(this).selectAll('.sequenceElements').remove();
                drawRow(this, d);
                drawRowPath(this, d);
            });



            // //Add userId and grade
            // subdata.forEach(function(d, i) {
            //     svgView.append('text')
            //         .attr('x', 0)
            //         .attr('y', function() {
            //             return i * 150 + 50;
            //         })
            //         .text(d.userId + " / " + d.grade)
            //         .style("fill", "black")
            //         .style("font-size", "15px");
            // });

        } else if (type == "ballon") {

            student_layout_rows = svgView.selectAll('.student')
                .data(subdata)
                .enter()
                .append('g')
                .attr('transform', (d, p) => 'translate(0,' + (p * 100 + 50) + ')');

            var xBottom = [],
                xUp = [];
            student_layout_rows.selectAll('path')
                .data(function(d) {
                    var rtd = [];
                    for (var i = 0; i < d.distance.length; ++i) {
                        t = {};
                        t["prev"] = d.sequence[i];
                        t["dis"] = d.distance[i];
                        t["last"] = d.sequence[i + 1];
                        rtd.push(t)
                    }
                    var x = 60,
                        str, tBottom = [],
                        tUp = [];
                    for (var i = 0; i < rtd.length; ++i) {
                        var flag = 0;
                        if (rtd[i]["prev"].includes("PF"))
                            flag += 1;
                        if (rtd[i]["last"].includes("PF"))
                            flag += 1;

                        str = "M ";
                        if (rtd[i].dis == 1) {
                            tBottom.push(x);
                            str += x + ' 0 L ' + x + " -25";
                            tUp.push(x);
                            x += 20;
                        }
                        if (rtd[i].dis < 1) {
                            if (flag == 1) {
                                tBottom.push(x);
                                str += x + ' 0 L ' + (x + 40) + ' -25';
                                tUp.push(x + 40);
                                x += 40 + 20;
                            } else {
                                tBottom.push(x);
                                str += x + ' 0 L ' + (x + 2.5 * Math.round(Math.sqrt(Math.abs(rtd[i].dis)))) + ' -25';
                                tUp.push(x + 2.5 * Math.round(Math.sqrt(Math.abs(rtd[i].dis))));
                                x += 2.5 * Math.round(Math.sqrt(Math.abs(rtd[i].dis))) + 20;
                            }
                        }
                        if (rtd[i].dis > 1) {
                            if (flag == 1) {
                                tUp.push(x);
                                str += x + ' -25 L ' + (x + 40) + ' 0';
                                tBottom.push(x + 40);
                                x += 40 + 20;
                            } else {
                                tUp.push(x);
                                str += x + ' -25 L ' + (x + 2.5 * Math.round(Math.sqrt(rtd[i].dis))) + ' 0';
                                tBottom.push(x + 2.5 * Math.round(Math.sqrt(rtd[i].dis)));
                                x += 2.5 * Math.round(Math.sqrt(rtd[i].dis)) + 20;
                            }
                        }
                        rtd[i]["path"] = str;
                    }
                    xBottom.push(tBottom);
                    xUp.push(tUp);
                    return rtd;
                })
                .enter()
                .append('path')
                //.attr("class", "link")
                .style("stroke", "black")
                .style("fill", "none")
                .attr('d', function(d, p) {
                    return d["path"];
                })
                .attr("class", function(d) {
                    var flag = 0;
                    if (d["prev"].includes("PF"))
                        flag += 1;
                    if (d["last"].includes("PF"))
                        flag += 1;
                    if (flag == 1)
                        return 'finalJump';
                });
            d3.selectAll(".finalJump").style("stroke-dasharray", "5,5");

            student_layout_rows.selectAll(".shapes")
                .data(function(d, p) {
                    var rtd = [];
                    for (var i = 0; i < d.sequence.length - 1; ++i) {
                        t = {};
                        t["TYPE"] = d.sequence[i];
                        t["x"] = xUp[p][i];
                        rtd.push(t);
                    }
                    return rtd;
                })
                .enter().append("path")
                .attr('d', d3.symbol().type(function(d) {
                    if (d["TYPE"][0] == "V")
                        return d3.symbolCircle;
                    else if (d["TYPE"][0] == "P")
                        return d3.symbolSquare;
                    else if (d["TYPE"][0] == "D")
                        return d3.symbolTriangle;
                }).size(180))
                .attr("transform", function(d) {
                    return "translate(" + d["x"] + ",-25)";
                })
                .attr("fill", d => color_week(element_to_week[overall.indexOf(d["TYPE"])] - 1))
                .style("opacity", "0.7")
                .style("stroke", function(d) {
                    if (d["TYPE"][0] == "P")
                        return "black";
                })
                .style("stroke-width", function(d) {
                    if (d["TYPE"][0] == "P")
                        return "1px";
                });

            student_layout_rows.selectAll('text')
                .data(function(d, p) {
                    var rtd = [];
                    for (var i = 0; i < d.sequence.length - 1; ++i) {
                        t = {};
                        t["TYPE"] = d.sequence[i];
                        t["x"] = xUp[p][i];
                        rtd.push(t);
                    }
                    return rtd;
                }).enter()
                .append('text')
                .attr('x', d => d["x"])
                .text(function(d) {
                    if (d["TYPE"][0] == "V")
                        return d["TYPE"].slice(1);
                    if (d["TYPE"][0] == "P") {
                        s = d["TYPE"].slice(2);
                        if (s[0] == "0")
                            return s.slice(1);
                        else
                            return s;
                    } else
                        return d["TYPE"];
                })
                .attr('y', -23)
                .attr("font-size", "8px")
                .attr("font-weight", "bold")
                .attr("fill", "#000")
                .attr("text-anchor", "middle");

            subdata.forEach(function(d, i) {
                svgView.append('text')
                    .attr('x', 0)
                    .attr('y', function() {
                        return i * 100;
                    })
                    .text(d.userId + " / " + d.grade)
                    .style("fill", "black")
                    .style("font-size", "15px");
            });

        }

        //Add Legend in the LegendDiv
        var series = [];
        for (var i = 1; i <= 11; ++i)
            series.push(i + '');
        var svgLegend = d3.select("#individualLegend").append("svg")
            .attr("width", "770px")
            .attr("height", "20px")
            .append("g");
        series.forEach(function(s, i) {
            svgLegend.append('text')
                .attr('x', i * 65 + 20)
                .attr('y', 10)
                .style("fill", "black")
                .attr("font-size", "10px")
                .text("W" + s);
            svgLegend.append('rect')
                .attr('fill', color_week(i))
                .attr('width', 30)
                .attr('height', 10)
                .attr('x', i * 65 + 40)
                .attr('y', 0);
        });

    }



    function drawIndividualCtrl(d) {

        d3.select("#individualControl svg").remove();
        d3.select("#individualControl").style("opacity", "0");

        var ctrlMargin = {
                top: 0,
                left: 20,
                bottom: 10,
                right: 10
            },
            ctrlWidth = 740 - ctrlMargin.left - ctrlMargin.right,
            ctrlHeight = 200 - ctrlMargin.top - ctrlMargin.bottom;

        var ageCondition = [],
            dateCondition = [],
            gradeCondition = [],
            lenCondition = [],
            simCondition = [];


        var svgControl = d3.select("#individualControl").append("svg")
            .attr("width", ctrlWidth + ctrlMargin.left + ctrlMargin.right)
            .attr("height", ctrlHeight + ctrlMargin.top + ctrlMargin.bottom)
            .attr("transform", "translate(0,0)")
            .append("g")
            .attr("transform", "translate(" + ctrlMargin.left + "," + ctrlMargin.top + ")");

        var xDate = d3.scaleTime()
            .domain([new Date(d.time.start * 1000), new Date(d.time.end * 1000)])
            .rangeRound([0, ctrlWidth]);
        var xLinear = d3.scaleLinear()
            .domain([0, 100])
            .range([0, ctrlWidth]);
        var domainLen = [];
        for (var i = 0; i <= 10; ++i)
            domainLen.push(i * 50 + '');
        domainLen.push("500+");
        var xLen = d3.scalePoint()
            .domain(domainLen)
            .range([0, ctrlWidth]);
        var lenLinear = d3.scaleLinear().domain([0, ctrlWidth]).range([0, 550]);


        let lengthBrushOffsetY = ctrlHeight - 100;
        let dateBrushOffsetY = ctrlHeight - 70;
        let gradeBrushOffsetY = ctrlHeight - 40;
        let ageBrushOffsetY = ctrlHeight - 10;
        let simirityBrushOffsetY = ctrlHeight - 130;

        //similarity
        let similarityLinear = d3.scaleLinear()
            .domain([0, 1])
            .range([0, ctrlWidth]);

        svgControl.append("g")
            .attr("class", "axis axis--grid")
            .attr("transform", "translate(0," + (simirityBrushOffsetY) + ")")
            .call(d3.axisBottom(similarityLinear)
                .ticks(20)
                .tickSize(-10)
                .tickFormat(function() {
                    return null;
                }));

        svgControl.append("g")
            .attr("transform", "translate(0," + (simirityBrushOffsetY) + ")")
            .call(d3.axisBottom(similarityLinear)
                .ticks(10)
                .tickPadding(0))
            .attr("text-anchor", "middle");

        svgControl.append("g")
            .attr("class", "brush")
            .attr("transform", "translate(0," + (simirityBrushOffsetY - 10) + ")")
            .call(d3.brushX()
                .extent([
                    [0, 0],
                    [ctrlWidth, 10]
                ])
                .on("end", similarityBrushend)
            );

        svgControl.append("text")
            .attr("transform", "translate(-10," + (simirityBrushOffsetY - 10) + ") rotate(90)")
            .attr("font-size", "10px")
            .text("Sim");

        function similarityBrushend(){
            if (!d3.event.sourceEvent) return;
            if (!d3.event.selection) return;
            var s = d3.event.selection;
            if (s == null) {
                return
            }
            console.log('ss', s);

            var s = d3.event.selection;
            if (s == null) {
                return
            }
            var extent = s.map(similarityLinear.invert);
            simCondition = extent;

            drawIndividualLines(getFiltered(dateCondition, gradeCondition, ageCondition, lenCondition, simCondition),
                individualType);


        }
        {// Length Brush
            svgControl.append("g")
                .attr("class", "axis axis--grid")
                .attr("transform", "translate(0," + (lengthBrushOffsetY) + ")")
                .call(d3.axisBottom(xLen)
                    .ticks(11)
                    .tickSize(-10)
                    .tickFormat(function() {
                        return null;
                    }));
            svgControl.append("g")
                .attr("transform", "translate(0," + (lengthBrushOffsetY) + ")")
                .call(d3.axisBottom(xLen)
                    .ticks(11)
                    .tickPadding(0))
                .attr("text-anchor", "middle");
            svgControl.append("g")
                .attr("class", "brush")
                .attr("transform", "translate(0," + (lengthBrushOffsetY - 10) + ")")
                .call(d3.brushX()
                    .extent([
                        [0, 0],
                        [ctrlWidth, 10]
                    ])
                    .on("end", lengthBrushend)
                );
            svgControl.append("text")
                .attr("transform", "translate(-10," + (lengthBrushOffsetY - 10) + ") rotate(90)")
                .attr("font-size", "10px")
                .text("Len")



            // Date Brush


            svgControl.append("g")
                .attr("class", "axis axis--grid")
                .attr("transform", "translate(0," + (dateBrushOffsetY) + ")")
                .call(d3.axisBottom(xDate)
                    .ticks(d3.timeDay)
                    .tickSize(-10)
                    .tickFormat(function() {
                        return null;
                    }));

            svgControl.append("g")
                .attr("transform", "translate(0," + (dateBrushOffsetY) + ")")
                .call(d3.axisBottom(xDate)
                    .ticks(d3.timeWeek)
                    .tickPadding(0)
                    .tickFormat(d3.timeFormat("%b %d")))
                .attr("text-anchor", "middle");

            svgControl.append("g")
                .attr("class", "brush")
                .attr("transform", "translate(0," + (dateBrushOffsetY) + ")")
                .call(d3.brushX()
                    .extent([
                        [0, 0],
                        [ctrlWidth, 10]
                    ])
                    .on("end", dateBrushend)
                );
            svgControl.append("text")
                .attr("transform", "translate(-10," + (dateBrushOffsetY) + ") rotate(90)")
                .attr("font-size", "10px")
                .text("Date")

            // Grade Brush
            svgControl.append("g")
                .attr("class", "axis axis--grid")
                .attr("transform", "translate(0," + (gradeBrushOffsetY) + ")")
                .call(d3.axisBottom(xLinear)
                    .ticks(20)
                    .tickSize(-10)
                    .tickFormat(function() {
                        return null;
                    }));
            svgControl.append("g")
                .attr("transform", "translate(0," + (gradeBrushOffsetY) + ")")
                .call(d3.axisBottom(xLinear)
                    .ticks(10)
                    .tickPadding(0))
                .attr("text-anchor", "middle");
            svgControl.append("g")
                .attr("class", "brush")
                .attr("transform", "translate(0," + (gradeBrushOffsetY - 10) + ")")
                .call(d3.brushX()
                    .extent([
                        [0, 0],
                        [ctrlWidth, 10]
                    ])
                    .on("end", gradeBrushend)
                );
            svgControl.append("text")
                .attr("transform", "translate(-10," + (gradeBrushOffsetY - 10) + ") rotate(90)")
                .attr("font-size", "10px")
                .text("Grade")

            // Age Brush


            svgControl.append("g")
                .attr("class", "axis axis--grid")
                .attr("transform", "translate(0," + (ageBrushOffsetY) + ")")
                .call(d3.axisBottom(xLinear)
                    .ticks(20)
                    .tickSize(-10)
                    .tickFormat(function() {
                        return null;
                    }));
            svgControl.append("g")
                .attr("transform", "translate(0," + (ageBrushOffsetY) + ")")
                .call(d3.axisBottom(xLinear)
                    .ticks(10)
                    .tickPadding(0))
                .attr("text-anchor", "middle");
            svgControl.append("g")
                .attr("class", "brush")
                .attr("transform", "translate(0," + (ageBrushOffsetY - 10) + ")")
                .call(d3.brushX()
                    .extent([
                        [0, 0],
                        [ctrlWidth, 10]
                    ])
                    .on("end", ageBrushend)
                );
            svgControl.append("text")
                .attr("transform", "translate(-10," + (ageBrushOffsetY - 10) + ") rotate(90)")
                .attr("font-size", "10px")
                .text("Age");


        }


        function lengthBrushend() {
            if (!d3.event.sourceEvent) return;
            if (!d3.event.selection) return;
            var d0 = [],
                d1 = [],
                moveto = [];
            d3.brushSelection(this).map(i => d0.push(Math.round(lenLinear(i))));
            d1.push(Math.floor(d0[0] / 50) * 50 + '');
            d1.push(Math.ceil(d0[1] / 50) * 50 + '');
            if (d1[1] == "550")
                d1[1] = "550+"
            d1.map(i => moveto.push(xLen(i)));
            if (moveto[1] == undefined)
                moveto[1] = ctrlWidth
            d3.select(this).transition().call(d3.event.target.move, moveto);
            lenCondition = d1;
            console.log("Individual Date selected");

            drawIndividualLines(getFiltered(dateCondition, gradeCondition, ageCondition, lenCondition, simCondition),
                individualType);
            filteredInfo(dateCondition, gradeCondition, ageCondition, lenCondition);
        }

        function ageBrushend() {
            if (!d3.event.sourceEvent) return; // Only transition after input.
            if (!d3.event.selection) return; // Ignore empty selections.
            ageCondition = d3.brushSelection(this).map(xLinear.invert).map(i => Math.round(i));
            console.log("Individual Age Selected");

            drawIndividualLines(getFiltered(dateCondition, gradeCondition, ageCondition, lenCondition, simCondition),
                individualType);
            filteredInfo(dateCondition, gradeCondition, ageCondition, lenCondition);
        }

        function gradeBrushend() {
            if (!d3.event.sourceEvent) return;
            if (!d3.event.selection) return;
            gradeCondition = d3.brushSelection(this).map(xLinear.invert).map(i => Math.round(i));
            console.log("Individual Grade Selected");
            console.log(gradeCondition);
            drawIndividualLines(getFiltered(dateCondition, gradeCondition, ageCondition, lenCondition, simCondition),
                individualType);
            filteredInfo(dateCondition, gradeCondition, ageCondition, lenCondition);
        }

        function dateBrushend() {
            if (!d3.event.sourceEvent) return;
            if (!d3.event.selection) return;
            var d0 = d3.event.selection.map(xDate.invert),
                d1 = d0.map(d3.timeDay.round);
            // If empty when rounded, use floor & ceil instead.
            if (d1[0] >= d1[1]) {
                d1[0] = d3.timeDay.floor(d0[0]);
                d1[1] = d3.timeDay.offset(d1[0]);
            }
            d3.select(this).transition().call(d3.event.target.move, d1.map(xDate));
            dateCondition = d1.map(i => Date.parse(i) / 1000);
            console.log("Individual Date selected");

            drawIndividualLines(getFiltered(dateCondition, gradeCondition, ageCondition, lenCondition, simCondition),
                individualType);
            filteredInfo(dateCondition, gradeCondition, ageCondition, lenCondition);
        }

        function getFiltered(date, grade, age, len, sim) {
            rtd = individualData;
            if (grade.length)
                rtd = _.filter(rtd, i => i.grade >= grade[0] && i.grade <= grade[1]);
            if (age.length)
                rtd = _.filter(rtd, i => parseInt(i.age) >= age[0] && parseInt(i.age) <= age[1]);
            if (date.length) {
                temp = [];
                rtd.forEach(function(i) {
                    var timeRange = _.filter(i["time"], o => o >= date[0] && o <= date[1]);
                    if (timeRange.length) {
                        var lower = i["time"].indexOf(timeRange[0]);
                        var upper = i["time"].indexOf(timeRange.slice(-1)[0]);
                        i["sequence"] = i["sequence"].slice(lower, upper + 1);
                        i["distance"] = i["distance"].slice(lower, upper);
                        i["distance_total"] = _.sum(i["distance"]);
                        i["time"] = timeRange;
                        temp.push(i)
                    }
                })
                rtd = temp;
            }
            if (len.length) {
                if (parseInt(len[1]) == 550)
                    rtd = _.filter(rtd, i => i["sequence"].length >= parseInt(len[0]));
                else
                    rtd = _.filter(rtd, i => i["sequence"].length >= parseInt(len[0]) && i["sequence"].length <=
                    parseInt(len[1]));
            }
            if(sim && sim.length){

                rtd = _.filter(rtd, function(d){
                    console.log('sign', simSign, sim)
                    if(simSign == false) return true;
                    if(d['individualDistance'] && d['individualDistance'] > sim[0]&&d['individualDistance'] < sim[1]){
                        return true
                    }
                    return false
                })
            }
            rtd = _.filter(rtd, i => i["sequence"].length > 0);
            return rtd
        }

        function filteredInfo(date, grade, age, len) {
            rtd = "";
            if (date.length) {
                rtd += "Date: " + new Date(date[0] * 1000).toDateString().slice(4, 10) + " to " + new Date(
                        date[
                            1] * 1000).toDateString().slice(4, 10) + " / ";
            }
            if (grade.length)
                rtd += "Grade: " + grade[0] + " to " + grade[1] + " / ";
            if (age.length)
                rtd += "Age: " + age[0] + " to " + age[1] + " / ";
            if (len.length)
                rtd += "Len: " + len[0] + " to " + len[1] + " / ";
            if (rtd.length) {
                rtd = rtd.slice(0, -2);
                d3.select("#individualLegend")
                    .append("text")
                    .text(rtd)
            }
        }

    }

}
