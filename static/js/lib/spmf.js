let drawSpmf = function(courseInfo, patternData) {

	d3.select("#spmfTitle").style("display", "block");
	d3.select("#spmfSelect").style("display", "block");

	var courseName = dataService.courseName;
	var overall = CourseConfig[courseName]['overall'];
	var element_to_week = CourseConfig[courseName]['element_to_week'];
	var colorDomain = CourseConfig[courseName]['colorDomain'];
	var colorRange = CourseConfig[courseName]['colorRange'];

	let over2IndexMap = {};
	overall.forEach(function(over, i) {
		over2IndexMap[over] = '' + i;
	});

	// CONTROL PANEL POPOVER
	$('#spmf-filter-brush').popover({
		template: $('#spmf-filter-brush-template').html()
	});
	$('#spmf-filter-brush').one('click', function(d) {
		drawPatternCtrl(courseInfo);
	});


	// Append Selection
	var slWeek = $("#selectWeek");
	var slLen = $("#selectLen");

	var weekSelected, lenSelected;

	let queryConditions = [];

	// modified by qiaomu
	var queryInput = $("#queryinput");
	var conditionAdd = $('#queryadd');
	var queryConfirm = $("#queryconfirm");
	var queryLabel = $("#querylabel")
		// used to record the data queried through week and number
	var currentData = [];


	$('#spmf-filter').popover({
		template: $('#spmf-filter-template').html()
	});
	$('#spmf-filter').one('click', function(d) {
		// Initialization of Popover
		$(".spmf-input").tagsinput('item');

		var week_select = d3.selectAll('.spmf-week-select');
		// week_select.selectAll('option').remove();
		week_select.selectAll('option').remove();
		week_select.selectAll('option')
			.data(d3.range(patternData.length))
			.enter()
			.append('option')
			.text(d => (d + 1))

		var length_select = d3.selectAll('.spmf-length-select');
		// length_select.selectAll('option').remove();
		length_select.selectAll('option').remove();
		length_select.selectAll('option')
			.data(d3.range(patternData.length))
			.enter()
			.append('option')
			.text(d => d)


		// $('.selectpicker').selectpicker();
		slWeek = $(".spmf-week-select").last();
		slLen = $(".spmf-length-select").last();


		slWeek.on("change", function(e) {
			var idx = parseInt(slWeek.val().slice(-1));
			if (idx == 0)
				idx = 10;
			weekSelected = idx - 1
			var data = patternData[weekSelected];


			var length_select = d3.selectAll('.spmf-length-select');
			// length_select.selectAll('option').remove();
			length_select.selectAll('option').remove();
			length_select.selectAll('option')
				.data(Object.keys(data))
				.enter()
				.append('option')
				.text(d => d)
		});
		slLen.on("change", function(e) {
			lenSelected = slLen.val();
			console.log('select', lenSelected)
			console.log('weekSelected,lenSelected')
			drawPatternBar(patternData, weekSelected, lenSelected);

			$('#spmf-redraw').on('click', function(d) {
				drawPatternBar(patternData, weekSelected, lenSelected);
			})
		});


		$('.spmf-go').on('click', function(e) {
			$('#spmf-filter').popover('hide')
			queryConditions = $(".spmf-input").last().tagsinput('items');
			console.log(queryConditions)
			console.log(currentData)
			let results = queryConditionFromSelectedData(currentData, queryConditions)
			console.log(results)
			drawPatterns(results, 10);

		})

	});


	var selected = [];
	var weekSelected = 1;
	var lenSelected = 8;
	drawPatternBar(patternData, weekSelected, lenSelected);
	$('#spmf-redraw').on('click', function(d) {
		drawPatternBar(patternData, weekSelected, lenSelected);
	})


	function drawPatternBar(raw, slWeek, slLen) {
		let data = raw[slWeek][slLen];
		currentData = data;
		drawPatterns(data, 20);
	}

	function drawPatterns(raw, slLen) {
		slLen = slLen == undefined ? 10 : slLen;
		d3.select("#spmfSvg svg").remove();

		randomArr = []
		for (var i = 0; i <= 100; ++i)
			randomArr.push(Math.floor(Math.random() * raw.length));
		randomIdx = _.uniq(randomArr);
		data = [];
		randomIdx.map(i => data.push(raw[i]))
		data.sort(function(a, b) {
			return b.sum - a.sum;
		});

		var element = document.getElementById("spmf");
		var computedStyle = getComputedStyle(element);
		var height = element.clientHeight; // height with padding
		var width = element.clientWidth; // width with padding
		height -= parseFloat(computedStyle.paddingTop) + parseFloat(computedStyle.paddingBottom);
		width -= parseFloat(computedStyle.paddingLeft) + parseFloat(computedStyle.paddingRight);

		var leftWid = 60,
			rightWid = (parseInt(slLen) + 2) * 35;
		var margin = {
			top: 10,
			right: 20,
			bottom: 10,
			left: 10
		};
		width -= margin.left + margin.right,
			height = data.length * 30 - margin.bottom - margin.top;

		var svg_parent = d3.select("#spmfSvg").append("svg")
			.attr("width", width + margin.left + margin.right)
			.attr("height", height + margin.top + margin.bottom)
		var svg = svg_parent.append("g")
			.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

		zoom = d3.zoom().scaleExtent([1, Infinity]).on('zoom', function() {
			return svg.attr('transform', d3.event.transform)
		});

		svg_parent.call(zoom)

		var tooltip = d3.select("#spmfSvg").append("div").attr("class", "toolTip");

		var x = d3.scaleLinear().domain([d3.min(data, function(d) {
			return d.sum;
		}) / 5, d3.max(data, function(d) {
			return d.sum;
		})]).range([0, leftWid]);

		var yInput = [];
		for (var i = 0; i < data.length; ++i)
			yInput.push(i);
		var gap = Math.round(height / yInput.length);
		var yOutput = [];
		for (var i = 0; i < yInput.length; ++i)
			yOutput.push(i * gap);
		var y = d3.scaleOrdinal().domain(yInput).range(yOutput);


		/*
		 svg.append("g")
		 .attr("class", "x axis")
		 .call(d3.axisTop(x).ticks(0).tickFormat(function() {
		 return null;
		 }));
		 */

		svg.append("g")
			.attr("class", "y axis")
			.attr("transform", "translate(" + leftWid + ",0)")
			.call(d3.axisRight(y).tickFormat(function() {
				return null;
			}));

		svg.selectAll(".bar")
			.data(data)
			.enter().append("rect")
			.attr("class", "bar")
			.attr("x", function(d) {
				return leftWid - x(d.sum);
			})
			.attr("y", function(d, p) {
				return y(p);
			})
			.attr("height", 20)
			.attr("width", function(d) {
				return x(d.sum);
			})
			.on("mousemove", function(d) {

				tooltip
					.style("left", d3.mouse(this)[0] - 50 + "px")
					.style("top", d3.mouse(this)[1] - 40 + "px")
					.style("display", "inline-block")
					.html(d.sum);
			})
			.on("mouseout", function(d) {
				tooltip.style("display", "none");
			});

		var color_week = d3.scaleQuantize()
			.domain(colorDomain)
			.range(colorRange);

		types = svg.selectAll('.types')
			.data(data)
			.enter()
			.append('g')
			.attr('transform', (d, p) => 'translate(' + leftWid + ',' + (y(p) + 10) + ')');

		types.selectAll(".shapes")
			.data(function(d) {
				return d["str"].map(i => overall[i])
			})
			.enter().append("path")
			.attr('d', d3.symbol().type(function(d) {
				if (d[0] == "V")
					return d3.symbolSquare;
				else if (d[0] == "P")
					return d3.symbolCircle;
				else if (d[0] == "D")
					return d3.symbolTriangle;
			}).size(250))
			.attr("transform", function(d, p) {
				return "translate(" + ((p + 1) * 30) + ",0)";
			})
			.attr("fill", d => color_week(element_to_week[overall.indexOf(d)] - 1))
			.style("opacity", "0.7")
			.style("stroke", function(d) {
				if (d[0] == "P")
					return "black";
			})
			.style("stroke-width", function(d) {
				if (d[0] == "P")
					return "2px";
			});

		types.selectAll('text').data(function(d) {
				return d["str"].map(i => overall[i])
			}).enter()
			.append('text')
			.attr('class', 'spmf-text')
			.attr('x', (d, p) => (p + 1) * 30)
			.text(d => d)
			.attr('y', 2)
			.style("font-size", "7px")
			.style("fill", function(d) {
				var bgColor = d3.color(color_week(element_to_week[overall.indexOf(d)] - 1)).rgb();
				var yiq = (bgColor.r * 299 + bgColor.g * 587 + bgColor.b * 114) / 1000;
				return (yiq >= 128) ? 'black' : 'white';
			})
			.style("text-anchor", "middle");
	}

	function queryConditionFromSelectedData(dataList, conditions) {
		if (conditions.length == 0) {
			return dataList;
		}
		let selected = [];
		dataList.forEach(function(data) {
			let strs = data['str'];
			let index = 0;
			let currentCondition = conditions[index];
			for (var i = 0, ilen = strs.length; i < ilen; i++) {
				if (over2IndexMap[currentCondition] == strs[i]) {
					index += 1;
					if (index == conditions.length) {
						selected.push(data);
						break;
					}
					currentCondition = conditions[index];
				}
			}

		});
		return selected;
	}

	function drawPatternCtrl(d) {

		d3.selectAll("#spmfControl svg").remove();
		// d3.selectAll("#spmfControl").style("opacity", "0");

		var ctrlMargin = {
				top: 0,
				left: 20,
				bottom: 10,
				right: 10
			},
			ctrlWidth = 600 - ctrlMargin.left - ctrlMargin.right,
			ctrlHeight = 110 - ctrlMargin.top - ctrlMargin.bottom;

		var ageCondition = [],
			dateCondition = [],
			gradeCondition = [],
			lenCondition = [];

		var svgControl = d3.selectAll("#spmfControl").append("svg")
			.attr("width", ctrlWidth + ctrlMargin.left + ctrlMargin.right)
			.attr("height", ctrlHeight + ctrlMargin.top + ctrlMargin.bottom)
			.attr("transform", "translate(0,0)")
			.append("g")
			.attr("transform", "translate(" + ctrlMargin.left + "," + ctrlMargin.top + ")");

		var xDate = d3.scaleTime()
			.domain([new Date(d.time.start * 1000), new Date(d.time.end * 1000)])
			.rangeRound([0, ctrlWidth]);
		var xLinear = d3.scaleLinear()
			.domain([0, 100])
			.range([0, ctrlWidth]);
		var domainLen = [];
		for (var i = 0; i <= 10; ++i)
			domainLen.push(i * 50 + '');
		domainLen.push("500+");
		var xLen = d3.scalePoint()
			.domain(domainLen)
			.range([0, ctrlWidth]);
		var lenLinear = d3.scaleLinear().domain([0, ctrlWidth]).range([0, 550]);

		// Date Brush
		svgControl.append("g")
			.attr("class", "axis axis--grid")
			.attr("transform", "translate(0," + (ctrlHeight - 60) + ")")
			.call(d3.axisBottom(xDate)
				.ticks(d3.timeDay)
				.tickSize(-20)
				.tickFormat(function() {
					return null;
				}));
		svgControl.append("g")
			.attr("transform", "translate(0," + (ctrlHeight - 60) + ")")
			.call(d3.axisBottom(xDate)
				.ticks(d3.timeWeek)
				.tickPadding(0)
				.tickFormat(d3.timeFormat("%b %d")))
			.attr("text-anchor", "middle");
		svgControl.append("g")
			.attr("class", "brush")
			.attr("transform", "translate(0," + (ctrlHeight - 80) + ")")
			.call(d3.brushX()
				.extent([
					[0, 0],
					[ctrlWidth, 20]
				])
			);
		svgControl.append("text")
			.attr("transform", "translate(-10," + (ctrlHeight - 80) + ") rotate(90)")
			.attr("font-size", "10px")
			.text("Date")

		// Grade Brush
		svgControl.append("g")
			.attr("class", "axis axis--grid")
			.attr("transform", "translate(0," + (ctrlHeight - 10) + ")")
			.call(d3.axisBottom(xLinear)
				.ticks(20)
				.tickSize(-20)
				.tickFormat(function() {
					return null;
				}));
		svgControl.append("g")
			.attr("transform", "translate(0," + (ctrlHeight - 10) + ")")
			.call(d3.axisBottom(xLinear)
				.ticks(10)
				.tickPadding(0))
			.attr("text-anchor", "middle");
		svgControl.append("g")
			.attr("class", "brush")
			.attr("transform", "translate(0," + (ctrlHeight - 30) + ")")
			.call(d3.brushX()
				.extent([
					[0, 0],
					[ctrlWidth, 20]
				])
			);
		svgControl.append("text")
			.attr("transform", "translate(-10," + (ctrlHeight - 30) + ") rotate(90)")
			.attr("font-size", "10px")
			.text("Grade")

		/*
		// Age Brush
		svgControl.append("g")
			.attr("class", "axis axis--grid")
			.attr("transform", "translate(0," + (ctrlHeight - 10) + ")")
			.call(d3.axisBottom(xLinear)
				.ticks(20)
				.tickSize(-10)
				.tickFormat(function() {
					return null;
				}));
		svgControl.append("g")
			.attr("transform", "translate(0," + (ctrlHeight - 10) + ")")
			.call(d3.axisBottom(xLinear)
				.ticks(10)
				.tickPadding(0))
			.attr("text-anchor", "middle");
		svgControl.append("g")
			.attr("class", "brush")
			.attr("transform", "translate(0," + (ctrlHeight - 20) + ")")
			.call(d3.brushX()
				.extent([
					[0, 0],
					[ctrlWidth, 10]
				])
				//.on("end", ageBrushend)
			);
		svgControl.append("text")
			.attr("transform", "translate(-10," + (ctrlHeight - 20) + ") rotate(90)")
			.attr("font-size", "10px")
			.text("Age");
		*/

	}

}
