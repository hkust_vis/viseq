var scatterView = new Vue({
	el: '#scatter',
	delimiters: ['{{', '}}'],
	data: {
		scatterData: null
	},
	mounted: function() {
		pipService.onScatterDataReady(function() {
			this.scatterData = dataService.getScatterData();
			drawScatter(this.scatterData);
		})
	}
});
