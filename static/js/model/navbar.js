var navbar = new Vue({
	el: '#body-navbar',
	delimiters: ['{{', '}}'],
	data: {
		buttons: [{
			'name': 'JAVA 2014',
			'link': "#"
		}, {
			'name': 'JAVA 2015',
			'link': "#"
		}, {
			'name': 'EBA 2014',
			'link': "#"
		}],
	},
	methods: {
		drawCourse: function(event) {
			course = event.target.text;
			if (course[0] == 'E')
				courseName = '2016';
			else
				courseName = course.slice(course.indexOf(" ") + 1);
			dataService.getScatterFromBackend(courseName);
			dataService.getPatternFromBackend(courseName);
			dataService.getAllChordFromBackend(courseName);
			//dataService.getIndividualFromBackend(courseName);
		}
	}
});
