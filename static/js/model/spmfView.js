var spmfView = new Vue({
    el: '#spmf',
    delimiters: ['{{', '}}'],
    data: {
        courseInfo: null,
        courseName: null,
        patternData: null,
        selected: []
    },
    mounted: function() {
        pipService.onPatternReady(function() {
            retrieveData = dataService.getPattern();
            this.courseInfo = retrieveData[0];
            this.patternData = retrieveData[1];
            drawSpmf(this.courseInfo, this.patternData);
        })
    }
});
