var individualView = new Vue({
	el: '#individual',
	delimiters: ['{{', '}}'],
	data: {
		courseName: null,
		courseInfo: null,
		individualData: null
	},
	mounted: function() {
		let _this = this;
		pipService.onIndividualReady(function() {
			retrieveData = dataService.getIndividual();
			this.courseInfo = retrieveData[0];
			this.individualData = retrieveData[1];
			drawIndividual(this.individualData, this.courseInfo);
		})
	}
});
