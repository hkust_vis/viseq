var chordView = new Vue({
	el: '#chord',
	delimiters: ['{{', '}}'],
	data: {
		courseInfo: null,
		courseName: null,
		chordWeek: null,
		chordInWeek: null,
	},
	mounted: function() {
		pipService.onAllChordReady(function() {
			retrieveData = dataService.getAllChordData();
			this.courseInfo = retrieveData[0];
			this.chordWeek = retrieveData[1];
			this.chordInWeek = retrieveData[2];
			drawChord(this.courseInfo, this.chordWeek, this.chordInWeek, "all");
		});
		pipService.onFilteredChordReady(function() {
			retrieveData = dataService.getFilteredChordData();
			this.chordWeek = retrieveData[0];
			this.chordInWeek = retrieveData[1];
			drawChord(this.courseInfo, this.chordWeek, this.chordInWeek, "filter");
		});
		pipService.onScatterFilteredChordReady(function() {
			retrieveData = dataService.getFilteredChordData();
			this.chordWeek = retrieveData[0];
			this.chordInWeek = retrieveData[1];
			drawChord(this.courseInfo, this.chordWeek, this.chordInWeek, "all");
		});
	}
});
