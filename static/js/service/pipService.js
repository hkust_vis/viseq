var pipService = new Vue({
	data: {
		COURSEREADY: 'choosecourse',
		PATTERNREADY: 'infodataready',
		INDIVIDUALREADY: 'individualready',
		INDIVIDUALSIMILARITY: 'individualsimilarity',
		CHORDREADY: 'chordready',
		CHORDFILTERED: 'chordfiltered',
		CHORDFILTEREDSCATTER: 'chordfilteredscatter'
	},
	methods: {
		emitScatterDataReady: function(msg) {
			this.$emit(this.COURSEREADY, msg);
		},
		onScatterDataReady: function(callback) {
			this.$on(this.COURSEREADY, function(msg) {
				callback(msg);
			});
		},
		emitPatternReady: function(msg) {
			this.$emit(this.PATTERNREADY, msg);
		},
		onPatternReady: function(callback) {
			this.$on(this.PATTERNREADY, function(msg) {
				callback(msg);
			});
		},
		emitAllChordReady: function(msg) {
			this.$emit(this.CHORDREADY, msg);
		},
		onAllChordReady: function(callback) {
			this.$on(this.CHORDREADY, function(msg) {
				callback(msg);
			});
		},
		emitFilteredChordReady: function(msg) {
			this.$emit(this.CHORDFILTERED, msg);
		},
		onFilteredChordReady: function(callback) {
			this.$on(this.CHORDFILTERED, function(msg) {
				callback(msg);
			});
		},
		emitScatterFilteredChordReady: function(msg) {
			this.$emit(this.CHORDFILTEREDSCATTER, msg);
		},
		onScatterFilteredChordReady: function(callback) {
			this.$on(this.CHORDFILTEREDSCATTER, function(msg) {
				callback(msg);
			});
		},
		emitIndividualReady: function(msg) {
			this.$emit(this.INDIVIDUALREADY, msg);
		},
		onIndividualReady: function(callback) {
			this.$on(this.INDIVIDUALREADY, function(msg) {
				callback(msg);
			});
		},
		emitIndividualSimilarityReady: function(msg) {
			this.$emit(this.INDIVIDUALSIMILARITY, msg);
		},
		onIndividualSimilarityReady: function(callback) {
			this.$on(this.INDIVIDUALSIMILARITY, function(msg) {
				callback(msg);
			});
		}

	}
})
