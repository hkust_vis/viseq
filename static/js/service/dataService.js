var dataService = new Vue({
	data: {
		scatterData: null,
		courseInfo: null,
		courseName: null,
		individualData: null,
		individualSimilarity: null,
		patternData: null,
		filteredChordWeek: null,
		filteredChorIndWeek: null,
		chordScaleDomain: [],
		chordPathClick: [],
		chordWeek: null,
		chordInWeek: null,
		chordAllWeek: null,
		chordAllInWeek: null
	},
	methods: {
		getScatterFromBackend: function(courseName) {
			$('#projection-spinner').show();
			this.$http.get('/getScatter', {
				params: {
					course: courseName
				}
			}).then(function(response) {
				$('#projection-spinner').hide();
				this.loading_scatter = false;
				this.scatterData = response.data;
				this.courseName = courseName;
				pipService.emitScatterDataReady();
			}, function(response) {
				$('#projection-spinner').hide();
				console.log('Get Scatter Data error');
			});
		},
		getScatterData: function() {
			if (this.scatterData != null)
				return this.scatterData;
			else
				return null;
		},
		getPatternFromBackend: function(courseName) {
			$('#pattern-spinner').show();
			this.$http.get('/getPattern', {
				params: {
					course: courseName
				}
			}).then(function(response) {
				$('#pattern-spinner').hide();
				this.courseInfo = response.data[0];
				this.patternData = response.data[1];
				this.courseName = courseName;
				pipService.emitPatternReady();
			}, function(response) {
				$('#pattern-spinner').hide();
				console.log('Get CourseInfo error');
			});
		},
		getPattern: function() {
			if (this.courseInfo != null && this.patternData != null)
				return [this.courseInfo, this.patternData];
			else
				return null;
		},
		getAllChordFromBackend: function(courseName) {
			$('#sequence-spinner').show();
			this.$http.get('/getAllChord', {
				params: {
					course: courseName
				}
			}).then(function(response) {
				$('#sequence-spinner').hide();
				this.courseInfo = response.data[0];
				this.chordAllWeek = this.chordWeek = response.data[1];
				this.chordAllInWeek = this.chordInWeek = response.data[2];
				this.courseName = courseName;

				var matrix = this.chordInWeek;
				matrix = Object.keys(matrix).map(key => matrix[key]);
				for (var i = 0; i < Object.keys(this.chordWeek).length; ++i)
					this.chordScaleDomain.push([d3.min(matrix[i].values, i =>
						d3.min(i)), d3.max(matrix[i].values,
						i => d3.max(i))]);

				pipService.emitAllChordReady();
			}, function(response) {
				$('#sequence-spinner').hide();
				console.log('Get Chord error');
			});
		},
		getAllChordData: function() {
			if (this.courseInfo != null && this.chordWeek != null && this.chordInWeek !=
				null)
				return [this.courseInfo, this.chordWeek, this.chordInWeek];
			else
				return null;
		},
		getFilteredChordFromBackend: function(type, para1, para2) {
			$('#sequence-spinner').show();
			if (type == "filterBrush") {
				parameters = {};
				if (para1.length) {
					parameters["dateStart"] = para1[0];
					parameters["dateEnd"] = para1[1];
				}
				if (para2.length) {
					parameters["gradeLower"] = para2[0];
					parameters["gradeUpper"] = para2[1];
				}
				this.$http.get('/getFilteredChordBrush', {
					params: parameters
				}).then(function(response) {
					$('#sequence-spinner').hide();
					this.filteredChordWeek = JSON.parse(response.data[0]);
					this.filteredChordInWeek = JSON.parse(response.data[1]);
					pipService.emitFilteredChordReady();
				}, function(response) {
					$('#sequence-spinner').hide();
					console.log('Get Filtered Chord Data error');
				});
			} else if (type == "pathClick") {
				parameters = {};
				parameters["from"] = para1;
				parameters["to"] = para2;
				this.chordPathClick.push(para1, para2);
				this.$http.get('/getFilteredChordPath', {
					params: parameters
				}).then(function(response) {
					$('#sequence-spinner').hide();
					this.filteredChordWeek = JSON.parse(response.data[0]);
					this.filteredChordInWeek = JSON.parse(response.data[1]);
					pipService.emitFilteredChordReady();
				}, function(response) {
					$('#sequence-spinner').hide();
					console.log('Get Filtered Chord Data error');
				});
			} else if (type == "scatterBrush") {
				parameters = {};
				parameters["para"] = JSON.stringify(para1);
				this.$http.get('/getFilteredChordScatter', {
					params: parameters
				}).then(function(response) {
					$('#sequence-spinner').hide();
					this.filteredChordWeek = JSON.parse(response.data[0]);
					this.filteredChordInWeek = JSON.parse(response.data[1]);
					pipService.emitScatterFilteredChordReady();
				}, function(response) {
					$('#sequence-spinner').hide();
					console.log('Get Filtered Chord Data error');
				});
			}
		},
		getFilteredChordData: function() {
			if (this.filteredChordWeek != null && this.filteredChordInWeek != null)
				return [this.filteredChordWeek, this.filteredChordInWeek];
			else
				return null;
		},
		getIndividualFromBackend: function(type, para, callback) {
			$('#individual-spinner').show();
			if (type == "chordUnit" || type == "chordFlow") {
				parameters = {};
				parameters["from"] = para[0];
				parameters["to"] = para[1];
				parameters["type"] = type;
				this.$http.get('/getIndividualFromChordClick', {
					params: parameters
				}).then(function(response) {
					$('#individual-spinner').hide();
					this.individualData = response.data[0];
					callback(response.data[1]);
					pipService.emitIndividualReady();
				}, function(response) {
					$('#individual-spinner').hide();
					console.log('Get Individual error');
				});
			} else if (type == "scatterGrade" || type == "scatterBrush") {
				parameters = {};
				parameters["para"] = JSON.stringify(para);
				parameters["type"] = type;
				this.$http.get('/getIndividualFromScatter', {
					params: parameters
				}).then(function(response) {
					$('#individual-spinner').hide();
					this.individualData = response.data;
					pipService.emitIndividualReady();
				}, function(response) {
					$('#individual-spinner').hide();
					console.log('Get Individual error');
				});
			}
		},
		getIndividual: function() {
			if (this.individualData != null)
				return [this.courseInfo, this.individualData];
			else
				return null;
		},
		getIndividualSimilarityFromBackend: function(id, callback) {
			$('#individual-spinner').show();
			this.$http.get('/getIndividualSimilarity', {
				params: {
					userId: id
				}
			}).then(function(response) {
				$('#individual-spinner').hide();
				this.individualSimilarity = response.data;
				callback(response.data);
				pipService.emitIndividualSimilarityReady();
			}, function(response) {
				$('#individual-spinner').hide();
				console.log('Get Individual Similarity error');
			});
		},
		getIndividualSimilarity: function() {
			if (this.individualSimilarity != null)
				return this.individualSimilarity;
			else
				return null;
		}

	},
	created: function() {
		//this.getGraphDataFromBackend();
	},
	watch: {
		graphData: {
			handler: function() {
				console.log('Graph data has been updated');
			},
			deep: true
		}
	}
})
