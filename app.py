from flask import Flask
from flask import request
import pandas as pd
import numpy as np
import copy
import json
import sys

app = Flask(__name__, static_url_path='')
@app.route('/')
def index():
    return app.send_static_file('index.html')

@app.route('/getScatter')
def getScatter():
    # Init chordFiltered here
    global chordFiltered
    chordFiltered = []

    course =  request.args.get("course")
    global currentCourse
    currentCourse = course
    fileName = "data/" + course[-2:] + "p.json"
    with open(fileName) as data_file:
        data = json.load(data_file)
        return json.dumps(data)


@app.route('/getPattern')
def getPattern():
    course =  request.args.get("course")
    rtd = []
    with open("data/courseInfo.json") as info_file:
        rtd.append(json.load(info_file)[course])
    fileName = "data/pattern" + course[-2:] + ".json"
    with open(fileName) as pattern:
        rtd.append(json.load(pattern))
    return json.dumps(rtd)


@app.route('/getIndividualFromChordClick')
def getIndividualFromChordClick():
    rtd = []

    print "CurrentCourse is : " + currentCourse
    print "----------Getting Individual Data from Chord Unit Clicking--------"
    clickFrom = str(request.args.get("from"))
    clickTo = str(request.args.get("to"))
    clickType = str(request.args.get("type"))

    print 'Click from ' + clickFrom + ' to ' + clickTo

    # Filter Data with parameters
    individualData = []
    fileName = "data/individual" + currentCourse[-2:] + ".json"
    with open(fileName) as data_file:
        individualData = json.load(data_file)

    if currentCourse == "2014":
        overall = overall14
        element_to_week = element_to_week14
    elif currentCourse == "2015":
        overall = overall15
        element_to_week = element_to_week15
    elif currentCourse == "2016":
        overall = overall16
        element_to_week = element_to_week16

    if clickType == "chordUnit":
        if len(chordFiltered):
            print "chordUnit with Filtered Chord"
            for i in chordFiltered:
                flag = 0
                for idx,val in enumerate(i['TYPE']):
                    if idx < len(i['TYPE']) - 1 and val == clickFrom and i['TYPE'][idx + 1] == clickTo:
                        for student in individualData:
                            if i["userId"] == student["userId"]:
                                rtd.append(student)
                                flag = 1
                                break
                    if flag == 1:
                        break
        else:
            print "chordUnit with All Data"
            for i in individualData:
                for idx,val in enumerate(i['sequence']):
                    if idx < len(i['sequence']) - 1 and val == clickFrom and i['sequence'][idx + 1] == clickTo:
                        rtd.append(i)
                        break
    elif clickType == "chordFlow":
        if len(chordFiltered):
            print "chordFlow with Filtered Chord"
            for i in chordFiltered:
                flag = 0
                for idx,val in enumerate(i['TYPE']):
                    if idx < len(i['TYPE']) - 1 and element_to_week[overall.index(val)] == int(clickFrom) and element_to_week[overall.index(i['TYPE'][idx + 1])] == int(clickTo):
                        for student in individualData:
                            if i["userId"] == student["userId"]:
                                rtd.append(student)
                                flag = 1
                                break
                    if flag == 1:
                        break
        else:
            print "chordFlow with All Data"
            for i in individualData:
                for idx,val in enumerate(i['sequence']):
                    if idx < len(i['sequence']) - 1 and element_to_week[overall.index(val)] == int(clickFrom) and element_to_week[overall.index(i['sequence'][idx + 1])] == int(clickTo):
                        rtd.append(i)
                        break
    print
    print "The Number of Students after filtering : " + str(len(rtd))
    print

    chordGrade = []
    for i in rtd:
        chordGrade.append(i["grade"])
    return json.dumps([rtd,chordGrade])

@app.route('/getIndividualFromScatter')
def getIndividualFromScatter():
    rtd = []

    print "CurrentCourse is : " + currentCourse
    print "----------Getting Individual Data from Scatter Brushing--------"

    individualData = []
    fileName = "data/individual" + currentCourse[-2:] + ".json"
    with open(fileName) as data_file:
        individualData = json.load(data_file)

    if currentCourse == "2014":
        overall = overall14
        element_to_week = element_to_week14
    elif currentCourse == "2015":
        overall = overall15
        element_to_week = element_to_week15
    elif currentCourse == "2016":
        overall = overall16
        element_to_week = element_to_week16

    brushType = request.args.get("type")
    if brushType == "scatterGrade":
        print '-------------------Brush the Scatter Grade-------------------'
        grade = request.args.get("para")[1:-1].split(',')
        for i in individualData:
            if i["grade"] >= int(grade[0]) and i["grade"] <= int(grade[1]):
                rtd.append(i)

    elif brushType == "scatterBrush":
        print '-------------------Brush the Scatter Points-------------------'
        selectedUsers = request.args.get("para")[1:-1].split(',')
        for i in individualData:
            if str(i["userId"]) in selectedUsers:
                rtd.append(i)

    print
    print "The Number of Students after Brushing : " + str(len(rtd))
    print
    return json.dumps(rtd)


def filterSimilarity(similarity, userId):
    for i in similarity:
        if i["userId"] == userId:
            l = i["val"]
            break
    rtd = []
    topIdx = sorted(range(len(l)), key=lambda i: l[i])
    for i in range(len(l)):
        t = {}
        t["userId"] = similarity[topIdx[i]]["userId"]
        t["val"] = l[topIdx[i]]
        rtd.append(t)
    return rtd
@app.route('/getIndividualSimilarity')
def getIndividualSimilarity():
    userId = int(request.args.get("userId"))
    if currentCourse == "2014":
        return json.dumps(filterSimilarity(similarity14, userId))
    elif currentCourse == "2015":
        return json.dumps(filterSimilarity(similarity15, userId))
    elif currentCourse == "2016":
        return json.dumps(filterSimilarity(similarity16, userId))


@app.route('/getAllChord')
def getAllChord():
    rtd = []
    course = request.args.get("course")
    with open("data/courseInfo.json") as info_file:
        rtd.append(json.load(info_file)[course])
    fileName = "data/chord/matrix_week" + course[-2:] + '.json'
    with open(fileName) as week_file:
        rtd.append(json.load(week_file))
    fileName2 = "data/chord/matrix_week_by_week" + course[-2:] + '.json'
    with open(fileName2) as week_week:
        rtd.append(json.load(week_week))
    return json.dumps(rtd)


def computeMatrix(newdata, overall, element_to_week, nb_week):
    rtd = []
    # Create Matrix overview week (FOR CHORD DIAGRAM)
    matrix_week = np.zeros((nb_week, nb_week),int)
    for student in newdata:
        last = ''
        for action in student['TYPE']:
            a = str(action)
            if a in overall and last in overall:
                last_id = overall.index(last)
                a_id = overall.index(a)
                last_id_week = element_to_week[a_id]
                a_id_week = element_to_week[last_id]
                matrix_week[last_id_week - 1][a_id_week - 1] += 1
            last = a

    matrix_week = pd.DataFrame.from_dict(matrix_week).to_json()
    rtd.append(matrix_week)
    print '---------------Got Matrix Week Data----------------------'

    # Select Top 5
    matrix_week_all_items = np.zeros((len(overall),len(overall)),int)
    for student in newdata:
        last = ''
        for action in student['TYPE']:
            a = str(action)
            if a in overall and last in overall:
                last_id = overall.index(last)
                a_id = overall.index(a)
                matrix_week_all_items[last_id][a_id] += 1
            last = a
    pd_matrix_week_all = pd.DataFrame.from_dict(matrix_week_all_items)

    rightAll = []
    rightTop = []
    rightTopValue = []
    for index,row in pd_matrix_week_all.iterrows():
        rtdList = []
        for rowIdx,rowVal in enumerate(list(row)):
            if element_to_week[rowIdx] == element_to_week[index]:
                rtdList.append(0)
            else:
                rtdList.append(rowVal)
        rightAll.append(rtdList)
    for l in rightAll:
        topIdx = sorted(range(len(l)), key=lambda i: l[i])[-5:][::-1]
        val = []
        name = []
        for i in topIdx:
            val.append(l[i])
            if l[i] == 0:
                name.append("")
            else:
                name.append(overall[i])
        rightTopValue.append(val)
        rightTop.append(name)
    leftAll = []
    leftTop = []
    leftTopValue = []
    for i in range(len(pd_matrix_week_all)):
        rtdList = []
        for rowIdx,rowVal in enumerate(list(pd_matrix_week_all[i])):
            if element_to_week[rowIdx] == element_to_week[i]:
                rtdList.append(0)
            else:
                rtdList.append(rowVal)
        leftAll.append(rtdList)
    for l in leftAll:
        topIdx = sorted(range(len(l)), key=lambda i: l[i])[-5:][::-1]
        val = []
        name = []
        for i in topIdx:
            val.append(l[i])
            if l[i] == 0:
                name.append("")
            else:
                name.append(overall[i])
        leftTopValue.append(val)
        leftTop.append(name)

    # Create Matrix overview week (FOR MATRIX VIEW)
    matrix_week_per_week = []
    for week in range(nb_week):
        nb_items = 0
        elements_in_this_week = []
        left_tops = []
        left_tops_values = []
        right_tops = []
        right_tops_values = []
        for (index,a) in enumerate(element_to_week):
            if a == week + 1:
                nb_items += 1
                elements_in_this_week.append(overall[index])
                left_tops.append(leftTop[index])
                left_tops_values.append(leftTopValue[index])
                right_tops.append(rightTop[index])
                right_tops_values.append(rightTopValue[index])
        item = {
            'week_nb': week,
            'values': np.zeros((nb_items,nb_items),int),
            'length': nb_items,
            'elements': elements_in_this_week,
            'leftTops':left_tops,
            'leftTopsVal':left_tops_values,
            'rightTops':right_tops,
            'rightTopsVal':right_tops_values
        }
        matrix_week_per_week.append(item)
    for student in newdata:
        last = ''
        for action in student['TYPE']:
            a = str(action)
            if a in overall and last in overall:
                last_id = overall.index(last)
                a_id = overall.index(a)
                last_id_week = element_to_week[a_id]
                a_id_week = element_to_week[last_id]
                if a_id_week == last_id_week: # Same week event
                    current_week = matrix_week_per_week[a_id_week - 1]
                    a_id_in_this_week = current_week['elements'].index(a)
                    last_id_in_this_week = current_week['elements'].index(last)
                    current_week['values'][last_id_in_this_week][a_id_in_this_week] += 1
            last = a

    matrix_week_per_week = pd.DataFrame.from_dict(matrix_week_per_week).T.to_json()
    rtd.append(matrix_week_per_week)
    print '---------------Got Matrix Week by Week Data--------------'
    return rtd

def filterChordBrush(seqData, gradeCondition, dateCondition, overall, element_to_week, nb_week):
    if len(chordFiltered):
        print '----------Filtering while Brushing Chord Filter with Filtered Data----------'
        tempData = chordFiltered
    else:
        print '----------Filtering while Brushing Chord Filter with All Data----------'
        tempData = seqData

    filterData = []
    for i in tempData:
        if len(gradeCondition):
            if i["grade"] < gradeCondition[0] or i["grade"] > gradeCondition[1]:
                continue
        if len(dateCondition):
            new_TYPE = []
            for idx,val in enumerate(i["time"]):
                if val >= dateCondition[0] and val <= dateCondition[1]:
                    new_TYPE.append(i["TYPE"][idx])
            result = {}
            result["TYPE"] = new_TYPE
            result["grade"] = i["grade"]
            result["time"] = i["time"]
            result['userId'] = i["userId"]
        filterData.append(result)
    print '---------------Finish Filtering Chord Data---------------'

    rtd = computeMatrix(filterData, overall, element_to_week, nb_week)
    print
    print "The Number of Students after Brushing : " + str(len(filterData))
    return rtd
@app.route('/getFilteredChordBrush')
def getFilteredChordBrush():
    keys = request.args.keys()
    date = []
    grade = []
    if len(keys) == 2:
        if "date" in keys[0]:
            date.append(int(request.args.get(keys[0])))
            date.append(int(request.args.get(keys[1])))
        if "grade" in keys[0]:
            grade.append(int(request.args.get(keys[1])))
            grade.append(int(request.args.get(keys[0])))
    if len(keys) == 4:
        date.append(int(request.args.get(keys[0])))
        date.append(int(request.args.get(keys[1])))
        grade.append(int(request.args.get(keys[2])))
        grade.append(int(request.args.get(keys[3])))

    if currentCourse == "2014":
        print 'Filtering JAVA 2014 Data'
        return json.dumps(filterChordBrush(seqData14, grade, date, overall14, element_to_week14, 11))
    elif currentCourse == "2015":
        print 'Filtering JAVA 2015 Data'
        return json.dumps(filterChordBrush(seqData15, grade, date, overall15, element_to_week15, 6))
    elif currentCourse == "2016":
        print 'Filtering EBA 2014 Data'
        return json.dumps(filterChordBrush(seqData16, grade, date, overall16, element_to_week16, 7))


def filterChordPath(seqData, pathFrom, pathTo, overall, element_to_week, nb_week):
    if len(chordFiltered):
        print '----------Filtering while Clicking Chord Path with Filtered Data----------'
        newdata = []
        for i in chordFiltered:
            for idx,val in enumerate(i['TYPE']):
                if idx < len(i['TYPE']) - 1 and val == pathFrom and i['TYPE'][idx + 1] == pathTo:
                    newdata.append(i)
                    break
    else:
        print '----------Filtering while Clicking Chord Path with All Data---------------'
        # Meet the Path Requirements
        newdata = []
        for i in seqData:
            for idx,val in enumerate(i['TYPE']):
                if idx < len(i['TYPE']) - 1 and val == pathFrom and i['TYPE'][idx + 1] == pathTo:
                    newdata.append(i)
                    break

    rtd = computeMatrix(newdata, overall, element_to_week, nb_week)
    # Use chordFiltered to filter people
    newChordFiltered = copy.deepcopy(newdata)
    rtd.append(newChordFiltered)
    print
    print "The Number of Students Containg This Path : " + str(len(newdata))
    return rtd
@app.route('/getFilteredChordPath')
def getFilteredChordPath():
    pathFrom = str(request.args.get("from"))
    pathTo = str(request.args.get("to"))
    if currentCourse == "2014":
        print 'Filtering JAVA 2014 Data'
        processedData = filterChordPath(seqData14, pathFrom, pathTo, overall14, element_to_week14, 11)
    elif currentCourse == "2015":
        print 'Filtering JAVA 2015 Data'
        processedData = filterChordPath(seqData15, pathFrom, pathTo, overall15, element_to_week15, 6)
    elif currentCourse == "2016":
        print 'Filtering EBA 2014 Data'
        processedData = filterChordPath(seqData16, pathFrom, pathTo, overall16, element_to_week16, 7)

    global chordFiltered
    chordFiltered = processedData[2]
    return json.dumps(processedData[:2])


def filterChordScatter(seqData, selected, overall, element_to_week, nb_week):
    filterData = []
    for i in seqData:
        if str(i["userId"]) in selected:
            filterData.append(i)
    print '---------------Finish Filtering Chord Data---------------'

    rtd = computeMatrix(filterData, overall, element_to_week, nb_week)

    newChordFiltered = copy.deepcopy(filterData)
    rtd.append(newChordFiltered)
    print
    print "The Number of Students after Brushing : " + str(len(filterData))
    return rtd
@app.route('/getFilteredChordScatter')
def getFilteredChordScatter():
    selectedUsers = request.args.get("para")[1:-1].split(',')
    if currentCourse == '2014':
        processedData = filterChordScatter(seqData14, selectedUsers, overall14, element_to_week14, 11)
    elif currentCourse == '2015':
        processedData = filterChordScatter(seqData15, selectedUsers, overall15, element_to_week14, 7)
    elif currentCourse == '2016':
        processedData = filterChordScatter(seqData16, selectedUsers, overall16, element_to_week16, 6)

    global chordFiltered
    chordFiltered = processedData[2]
    return json.dumps(processedData[:2])


if __name__ == '__main__':

    global chordFiltered
    global currentCourse
    global seqData14
    global seqData15
    global seqData16
    global similarity14
    global similarity15
    global similarity16

    with open('data/allSeq14.json') as data_file:
        seqData14 = json.load(data_file)
    # with open('data/allSeq15.json') as data_file:
    #     seqData15 = json.load(data_file)
    # with open('data/allSeq16.json') as data_file:
    #     seqData16 = json.load(data_file)
    with open('data/similarity14.json') as data_file:
        similarity14 = json.load(data_file)
    # with open('data/similarity15.json') as data_file:
    #     similarity15 = json.load(data_file)
    # with open('data/similarity16.json') as data_file:
    #     similarity16 = json.load(data_file)

    overall14 = ['V1.1', 'V1.2', 'V1.3', 'D1', 'V1.4', 'V1.5', 'V1.6', 'V1.7', 'V1.8', 'V1.9', 'V1.10', 'V1.11', 'V1.12', 'PW01.1', 'PW01.2', 'V1.13', 'V1.14', 'PL01.1', 'V2.1', 'V2.2', 'V2.3', 'PW02.1', 'V2.4', 'PW02.2', 'V2.5', 'V2.6', 'V2.7', 'PW02.3', 'V2.8', 'V2.9', 'V2.10', 'PW02.4', 'D2', 'V2.11', 'V2.12', 'PL02.1', 'PL02.2', 'PL02.3', 'V3.1', 'V3.2', 'V3.3', 'V3.4', 'V3.5', 'V3.6', 'PW03.1', 'D3.1', 'V3.7', 'D3.2', 'V3.8', 'V3.9', 'V3.10', 'V3.11', 'V3.12', 'PW03.2', 'V3.13', 'V3.14', 'V3.15', 'PW03.3', 'PW03.4', 'PL03.1', 'PL03.2', 'PL03.3', 'PL03.4', 'V4.1', 'V4.2', 'V4.3', 'PW04.1', 'V4.4', 'V4.5', 'V4.6', 'V4.7', 'D4', 'V4.8', 'PW04.2', 'V4.9', 'V4.10', 'V4.11', 'V4.12', 'PW04.3', 'PW04.4', 'V5.1', 'V5.2', 'V5.3', 'PW05.1', 'V5.4', 'V5.5', 'V5.6', 'PW05.2', 'PW05.3', 'V5.7', 'PW05.4', 'PW05.5', 'V5.8', 'D5', 'V5.9', 'V5.10', 'V5.11', 'V5.12', 'V5.13', 'PL04.1', 'PL04.2', 'PL04.3', 'PL04.4', 'V6.1', 'V6.2', 'PW06.1', 'V6.3', 'PW06.2', 'D6', 'V6.4', 'PW06.3', 'V6.5', 'V6.6', 'V6.7', 'PW06.4', 'V6.8', 'PW06.5', 'V6.9', 'PW06.6', 'V6.10', 'V6.11', 'V7.1', 'V7.2', 'V7.3', 'PW07.1', 'V7.4', 'PW07.2', 'V7.5', 'PW07.3', 'V7.6', 'V7.7', 'V7.8', 'V7.9', 'PW07.4', 'V7.10', 'V7.11', 'PL05.1', 'PL05.2', 'PL05.3', 'PL05.4', 'V8.1', 'V8.2', 'V8.3', 'PW08.1', 'V8.4', 'V8.5', 'PW08.2', 'V8.6', 'V8.7', 'V8.8', 'V8.9', 'V8.10', 'V8.11', 'PW08.3', 'PL06.1', 'PL06.2', 'PL06.3', 'PL06.4', 'PL06.5', 'V9.1', 'V9.2', 'V9.3', 'PW09.1', 'V9.4', 'PW09.2', 'V9.5', 'PW09.3', 'D9', 'V9.6', 'V9.7', 'V9.8', 'V9.9', 'V9.10', 'V9.11', 'PW09.4', 'V10.1', 'V10.2', 'V10.3', 'V10.4', 'PW10.1', 'V10.5', 'V10.6', 'V10.7', 'V10.8', 'V10.9', 'PW10.2', 'V10.10', 'V10.11', 'V10.12', 'PF01', 'PF02', 'PF03', 'PF04', 'PF05', 'PF06', 'PF07', 'PF08', 'PF09', 'PF10', 'PF11', 'PF12', 'PF13', 'PF14', 'PF15', 'PF16', 'PF17', 'PF18', 'PF19', 'PF20', 'PF21', 'PF22', 'PF23', 'PF24', 'PF25', 'PF26', 'PF27', 'PF28', 'PF29', 'PF30']
    overall15 = ['V1.1', 'V1.2', 'V1.3', 'V1.4', 'V1.5', 'V1.6', 'V1.7', 'V1.8', 'V1.9', 'V1.10', 'V1.11', 'PW01.1', 'PW01.2', 'V1.12', 'V1.13', 'PL01.1', 'V2.1', 'V2.2', 'V2.3', 'PW02.1', 'V2.4', 'PW02.2', 'V2.5', 'V2.6', 'V2.7', 'PW02.3', 'V2.8', 'V2.9', 'V2.10', 'PW02.4', 'D2', 'V2.11', 'V2.12', 'PL02.1', 'PL02.2', 'PL02.3', 'V3.1', 'V3.2', 'V3.3', 'V3.4', 'V3.5', 'V3.6', 'PW03.1', 'D3.1', 'V3.7', 'D3.2', 'V3.8', 'V3.9', 'V3.10', 'V3.11', 'V3.12', 'PW03.2', 'V3.13', 'V3.14', 'V3.15', 'PW03.3', 'PW03.4', 'PL03.1', 'PL03.2', 'PL03.3', 'PL03.4', 'V4.1', 'V4.2', 'V4.3', 'PW04.1', 'V4.4', 'V4.5', 'V4.6', 'V4.7', 'D4', 'V4.8', 'V4.9', 'PW04.2', 'V4.10', 'V4.11', 'V4.12', 'PW04.3', 'PW04.4', 'V4.13', 'PL04.1', 'PL04.2', 'PL04.3', 'V5.1', 'V5.2', 'V5.3', 'PW05.1', 'V5.4', 'V5.5', 'V5.6', 'PW05.2', 'PW05.3', 'V5.7', 'PW05.4', 'PW05.5', 'V5.8', 'D5.1', 'V5.8', 'V5.9', 'PW05.6', 'V5.10', 'PW05.7', 'D5.2', 'V5.11', 'PW05.8', 'PL05.1', 'PL05.2', 'PF01', 'PF02', 'PF03', 'PF04', 'PF05', 'PF06', 'PF07', 'PF08', 'PF09', 'PF10', 'PF11a', 'PF11b', 'PF11c', 'PF11d', 'PF11e', 'PF12A', 'PF12B', 'PF12C', 'PF12D', 'PF12E', 'PF12F', 'PF12G', 'PF12H', 'PF12I', 'PF12J', 'PF13A', 'PF13B', 'PF13C', 'PF13D', 'PF14', 'PF15A', 'PF15B']
    overall16 = ['V1.1', 'V1.2', 'V1.3', 'V1.4', 'PA1', 'D1', 'V1.5', 'V1.6', 'V1.7', 'V1.8', 'PA2', 'PA3', 'PA4', 'PA5', 'PA6', 'PA7', 'PA8', 'PA9', 'PA10', 'PA11', 'PA12', 'PA13', 'PQ1', 'PA14', 'PA15', 'PA16', 'PA17', 'V2.1', 'PA18', 'D2.1', 'V2.2', 'PA19', 'D2.2', 'V2.3', 'PA20', 'D2.3', 'V2.4', 'V2.5', 'PA21', 'PA22', 'PA23', 'PA24', 'PA25', 'PA26', 'PA27', 'PA28', 'PA29', 'PA30', 'PA31', 'PA32', 'PA33', 'PA34', 'PQ2', 'V3.1', 'PA35', 'PA36', 'PA37', 'PA38', 'PA39', 'PA40', 'PA41', 'PA42', 'V3.2', 'V3.3', 'PA43', 'V3.4', 'V3.5', 'PA44', 'PA45', 'PA46', 'PA47', 'PA48', 'PA49', 'PA50', 'PA51', 'PA52', 'PA53', 'PA54', 'PA55', 'PQ3', 'V4.1', 'PA56', 'V4.2', 'V4.3', 'PA57', 'V4.4', 'V4.5', 'V4.6', 'V4.7', 'V4.8', 'PA58', 'V4.9', 'V4.10', 'PA59', 'PA60', 'PA61', 'PA62', 'PA63', 'PA64', 'PA65', 'PA66', 'PA67', 'PA68', 'PA69', 'PA70', 'V4.11', 'V5.1', 'V5.2', 'PA71', 'D5', 'V5.3', 'PA72', 'V5.4', 'PA73', 'V5.5', 'V5.6', 'PA74', 'V5.7', 'V5.8', 'PA75', 'PA76', 'PA77', 'PA78', 'PA79', 'PA80', 'PA81', 'PA82', 'PA83', 'PA84', 'PA85', 'PA86', 'V5.9', 'PA87', 'V6.1', 'V6.2', 'V6.3', 'V6.4', 'V6.5', 'D6', 'V6.6', 'V6.7', 'PA88', 'PA89', 'PA90', 'PA91', 'PA92', 'PA93', 'PA94', 'PA95', 'PA96', 'PA97', 'PA98', 'V6.8', 'PF']
    element_to_week14 = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11]
    element_to_week15 = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6]
    element_to_week16 = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 7]


    app.run(debug = True)
